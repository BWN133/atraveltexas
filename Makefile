SHELL 		:= bash


install:
	cd front-end && npm install

run-frontend:
	cd front-end && npm install && yarn start

back_run:
	cd back-end/ && eb local run --port 5000

tests:
	echo "Running python unit tests..."
	python3 back-end/test.py -v

docker:
	docker run --rm -i -t -v $(PWD):/usr/python -w /usr/python gpdowning/python

format:
	black back-end/app.py
	black back-end/test.py

selenium-tests:
	chmod 775 selenium/chromedriver.exe
	chmod 775 selenium/chromedriver_linux
	python3 selenium/guitests.py