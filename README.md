# ATravelTexas

Names : Bowen Yang, Eric Chen, Kelly Flores, Yuhan Gao

EIDs : by3635, elc2637, kmf923, yg6952

Gitlab IDs : bwn133, ericlchen, kellyflores, yhgaoFAGBAT

Git SHA : c1aa9020aa6d89785c15df969e8df37302ba3448

Phase 1 project leader : Eric Chen

Gitlab pipeline : https://gitlab.com/BWN133/atraveltexas/-/pipelines

Website Link : https://atraveltx.me

estimated completion time(hours : int) :

- Bowen Yang -- 10
- Eric Chen -- 15
- Kelly Flores -- 15
- Yuhan Gao -- 10

actual completion time(hours : int) :

- Bowen Yang -- 15
- Eric Chen -- 17
- Kelly Flores -- 15 
- Yuhan Gao -- 12


Phase 2 project leader : Yuhan Gao

estimated completion time(hours : int) :

- Bowen Yang -- 15
- Eric Chen -- 15
- Kelly Flores -- 15
- Yuhan Gao -- 15

actual completion time(hours : int) :

- Bowen Yang -- 25
- Eric Chen -- 30
- Kelly Flores -- 27
- Yuhan Gao -- 25

Phase 3 project leader: Bowen Yang

estimated completion time(hours : int) :

- Bowen Yang -- 15
- Eric Chen -- 15
- Kelly Flores -- 15
- Yuhan Gao -- 15

actual completion time(hours : int) :

- Bowen Yang -- 25
- Eric Chen -- 25
- Kelly Flores -- 10
- Yuhan Gao -- 20

Phase 4 project leader: Kelly Flores

estimated completion time(hours : int) :

- Bowen Yang -- 10
- Eric Chen -- 10
- Kelly Flores -- 10
- Yuhan Gao -- 10

actual completion time(hours : int) :

- Bowen Yang -- 5
- Eric Chen -- 5
- Kelly Flores -- 5
- Yuhan Gao -- 5

Comments :