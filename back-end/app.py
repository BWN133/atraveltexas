from pickle import NONE
from flask import Flask, request, jsonify, json
from sqlalchemy.sql.type_api import NULLTYPE
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import create_engine, Column, String, Integer, JSON, or_, desc, asc
from flask_marshmallow import Marshmallow
from flask_cors import CORS
import urllib

# import json


app = Flask(__name__)
CORS(app)
app.debug = True
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
# Schema: "postgres+psycopg2://<USERNAME>:<PASSWORD>@<IP_ADDRESS>:<PORT>/<DATABASE_NAME>"
app.config[
    "SQLALCHEMY_DATABASE_URI"
] = "mysql+mysqlconnector://atraveltx:ATravelTX123!!@atraveltx.crgzfpjluweb.us-east-2.rds.amazonaws.com:3306/travel"
db = SQLAlchemy(app)
ma = Marshmallow(app)


class City(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    city_name = db.Column(db.String())
    city_number = db.Column(db.String())
    Have_county = db.Column(db.String())
    population = db.Column(db.String())
    Longitude = db.Column(db.Float)
    Latitude = db.Column(db.Float)
    County_name = db.Column(db.String())
    sale_tax = db.Column(db.String())
    size = db.Column(db.String())
    wiki_link = db.Column(db.String())
    wiki_history = db.Column(db.String())
    average_temp = db.Column(db.Float)
    position = db.Column(db.String())

    def __init__(
        self,
        id,
        city_name,
        city_number,
        Have_county,
        population,
        Longitude,
        Latitude,
        County_name,
        sale_tax,
        size,
        wiki_link,
        wiki_history,
        average_temp,
        position,
    ):
        self.id = id
        self.city_name = city_name
        self.city_number = city_number
        self.Have_county = Have_county
        self.population = population
        self.Longitude = Longitude
        self.Latitude = Latitude
        self.County_name = County_name
        self.sale_tax = sale_tax
        self.size = size
        self.wiki_link = wiki_link
        self.wiki_history = wiki_history
        self.average_temp = average_temp
        self.position = position


class CitySchema(ma.SQLAlchemySchema):
    class Meta:
        fields = (
            "id",
            "city_name",
            "city_number",
            "Have_county",
            "population",
            "Longitude",
            "Latitude",
            "County_name",
            "sale_tax",
            "size",
            "wiki_link",
            "wiki_history",
            "average_temp",
            "position",
        )


class Us_cities(db.Model):
    ID = db.Column(db.Integer, primary_key=True)
    STATE_CODE = db.Column(db.String())
    STATE_NAME = db.Column(db.String())
    CITY = db.Column(db.String())
    COUNTY = db.Column(db.String())
    LATITUDE = db.Column(db.Float)
    LONGITUDE = db.Column(db.Float)

    def __init__(self, ID, STATE_CODE, STATE_NAME, CITY, LATITUDE, LONGITUDE):
        self.ID = ID
        self.STATE_CODE = STATE_CODE
        self.STATE_NAME = STATE_NAME
        self.CITY = CITY
        self.LATITUDE = LATITUDE
        self.LONGITUDE = LONGITUDE


class Us_citiesSchema(ma.SQLAlchemySchema):
    class Meta:
        fields = ("ID", "STATE_CODE", "STATE_NAME", "CITY", "LATITUDE", "LONGITUDE")


class Flight_combined(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    flight_date = db.Column(db.String())
    departure_airport = db.Column(db.String())
    departure_iata = db.Column(db.String())
    departure_icao = db.Column(db.String())
    departure_scheduled = db.Column(db.String())
    departure_terminal = db.Column(db.String())
    departure_gate = db.Column(db.String())
    arrival_airport = db.Column(db.String())
    arrival_iata = db.Column(db.String())
    arrival_icao = db.Column(db.String())
    arrival_scheduled = db.Column(db.String())
    arrival_terminal = db.Column(db.String())
    arrival_gate = db.Column(db.String())
    arrival_baggage = db.Column(db.String())
    airline_name = db.Column(db.String())
    airline_iata = db.Column(db.String())
    airline_icao = db.Column(db.String())
    flight_number = db.Column(db.String())
    flight_iata = db.Column(db.String())
    flight_icao = db.Column(db.String())
    codeshared_name = db.Column(db.String())
    codeshared_number = db.Column(db.String())
    codeshared_iata = db.Column(db.String())
    codeshared_icao = db.Column(db.String())
    name = db.Column(db.String())
    elevation = db.Column(db.Float)
    city = db.Column(db.String())
    state = db.Column(db.String())
    country_code = db.Column(db.String())
    longitude = db.Column(db.String())
    latitude = db.Column(db.String())
    timezone = db.Column(db.String())
    wiki_url = db.Column(db.String())
    county = db.Column(db.String())

    def __init__(
        self,
        id,
        flight_date,
        departure_airport,
        departure_iata,
        departure_icao,
        departure_scheduled,
        departure_terminal,
        departure_gate,
        arrival_airport,
        arrival_iata,
        arrival_icao,
        arrival_scheduled,
        arrival_terminal,
        arrival_gate,
        arrival_baggage,
        airline_name,
        airline_iata,
        airline_icao,
        flight_number,
        flight_iata,
        flight_icao,
        codeshared_name,
        codeshared_number,
        codeshared_iata,
        codeshared_icao,
        name,
        elevation,
        city,
        state,
        country_code,
        longitude,
        latitude,
        timezone,
        wiki_url,
        county,
    ):
        self.id = id
        self.flight_date = flight_date
        self.departure_airport = departure_airport
        self.departure_iata = departure_iata
        self.departure_icao = departure_icao
        self.departure_scheduled = departure_scheduled
        self.departure_terminal = departure_terminal
        self.departure_gate = departure_gate
        self.arrival_airport = arrival_airport
        self.arrival_iata = arrival_iata
        self.arrival_icao = arrival_icao
        self.arrival_scheduled = arrival_scheduled
        self.arrival_terminal = arrival_terminal
        self.arrival_gate = arrival_gate
        self.arrival_baggage = arrival_baggage
        self.airline_name = airline_name
        self.airline_iata = airline_iata
        self.airline_icao = airline_icao
        self.flight_date = flight_date
        self.flight_iata = flight_iata
        self.flight_number = flight_number
        self.flight_icao = flight_icao
        self.codeshared_iata = codeshared_iata
        self.codeshared_name = codeshared_name
        self.codeshared_number = codeshared_number
        self.codeshared_icao = codeshared_icao
        self.name = name
        self.elevation = elevation
        self.city = city
        self.state = state
        self.country_code = country_code
        self.latitude = latitude
        self.longitude = longitude
        self.timezone = timezone
        self.wiki_url = wiki_url
        self.county = county


class Flight_combinedSchema(ma.SQLAlchemySchema):
    class Meta:
        fields = (
            "id",
            "flight_date",
            "departure_airport",
            "departure_iata",
            "departure_icao",
            "departure_scheduled",
            "departure_terminal",
            "departure_gate",
            "arrival_airport",
            "arrival_iata",
            "arrival_icao",
            "arrival_scheduled",
            "arrival_terminal",
            "arrival_gate",
            "arrival_baggage",
            "airline_name",
            "airline_iata",
            "airline_icao",
            "flight_number",
            "flight_iata",
            "flight_icao",
            "codeshared_name",
            "codeshared_number",
            "codeshared_iata",
            "codeshared_icao",
            "name",
            "elevation",
            "city",
            "state",
            "country_code",
            "longitude",
            "latitude",
            "timezone",
            "wiki_url",
            "county",
        )


class Covid(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    county_name = db.Column(db.String())
    cases = db.Column(db.Integer)
    deaths = db.Column(db.Integer)
    test_positivity_ratio = db.Column(db.Float)
    case_density = db.Column(db.Float)
    infection_rate = db.Column(db.Float)
    vaccinations_initiated_ratio = db.Column(db.Float)
    vaccinations_completed_ratio = db.Column(db.Float)
    overall_risk_level_IGNORE = db.Column(db.Float)
    cdc_transmission_level_IGNORE = db.Column(db.Integer)
    icu_bed_capacity = db.Column(db.Integer)
    icu_bed_current_usage = db.Column(db.Integer)
    cdc_transmission_level = db.Column(db.String())
    risk_level = db.Column(db.String())
    fips = db.Column(db.Integer)
    values_time_series = db.Column(db.JSON)
    population = db.Column(db.String())

    def __init__(
        self,
        id,
        county_name,
        cases,
        deaths,
        test_positivity_ratio,
        case_density,
        infection_rate,
        vaccinations_initiated_ratio,
        vaccinations_completed_ratio,
        overall_risk_level_IGNORE,
        cdc_transmission_level_IGNORE,
        icu_bed_capacity,
        icu_bed_current_usage,
        cdc_transmission_level,
        risk_level,
        fips,
        values_time_series,
        population,
    ):
        self.fips = fips
        self.risk_level = risk_level
        self.cdc_transmission_level = cdc_transmission_level
        self.icu_bed_current_usage = icu_bed_current_usage
        self.icu_bed_capacity = icu_bed_capacity
        self.cdc_transmission_level_IGNORE = cdc_transmission_level_IGNORE
        self.overall_risk_level_IGNORE = overall_risk_level_IGNORE
        self.vaccinations_completed_ratio = vaccinations_completed_ratio
        self.vaccinations_initiated_ratio = vaccinations_initiated_ratio
        self.infection_rate = infection_rate
        self.case_density = case_density
        self.test_positivity_ratio = test_positivity_ratio
        self.deaths = deaths
        self.cases = cases
        self.county_name = county_name
        self.id = id
        self.values_time_series = values_time_series
        self.population = population


class CovidSchema(ma.SQLAlchemySchema):
    class Meta:
        fields = (
            "id",
            "county_name",
            "cases",
            "deaths",
            "test_positivity_ratio",
            "case_density",
            "infection_rate",
            "vaccinations_initiated_ratio",
            "vaccinations_completed_ratio",
            "overall_risk_level_IGNORE",
            "cdc_transmission_level_IGNORE",
            "icu_bed_capacity",
            "icu_bed_current_usage",
            "cdc_transmission_level",
            "risk_level",
            "fips",
            "values_time_series",
            "population",
        )


covid_schema = CovidSchema()
covids_schema = CovidSchema(many=True)

flight_schema = Flight_combinedSchema()
flights_schema = Flight_combinedSchema(many=True)

us_citiy_schema = Us_citiesSchema()
us_cities_schema = Us_citiesSchema(many=True)

city_schema = CitySchema()
cities_schema = CitySchema(many=True)


@app.route("/")
def hello_world():
    return "ok"


# get all covid stats
@app.route("/api/covid", methods=["GET"])
def covid():
    result = db.session.query(Covid)
    searching_flag = request.args.get("f")
    if searching_flag != None:
        search = []
        searching_words = searching_flag.split(" ")
        for word in searching_words:
            search.append(Covid.county_name.contains(word))
            search.append(Covid.cases.contains(word))
            search.append(Covid.deaths.contains(word))
            search.append(Covid.infection_rate.contains(word))
            search.append(Covid.risk_level.contains(word))
            search.append(Covid.icu_bed_capacity.contains(word))
            search.append(Covid.icu_bed_current_usage.contains(word))
            search.append(Covid.population.contains(word))
            search.append(Covid.case_density.contains(word))

        result = result.filter(or_(*(search)))

    f_cases = []
    f_deaths = []
    f_risk_level = []
    f_transmission_level = []
    f_infection_rate = []
    f_icu_bed_capacity = []
    f_icu_bed_usage = []

    filter_cases = request.args.get("c")
    filter_deaths = request.args.get("d")
    filter_risk_level = request.args.get("rl")
    filter_transmission_level = request.args.get("tl")
    filter_infection_rate = request.args.get("ir")
    filter_icu_bed_capacity = request.args.get("ibc")
    filter_icu_bed_usage = request.args.get("ibu")

    if filter_cases != None:
        if filter_cases == "1":
            f_cases.extend({Covid.cases.between(0, 1000)})
        elif filter_cases == "2":
            f_cases.extend({Covid.cases.between(1000, 5000)})
        elif filter_cases == "3":
            f_cases.extend({Covid.cases.between(5000, 10000)})
        else:
            f_cases.extend({Covid.cases > 10000})

    if filter_deaths != None:
        if filter_deaths == "1":
            f_deaths.extend({Covid.deaths.between(0, 20)})
        elif filter_deaths == "2":
            f_deaths.extend({Covid.deaths.between(20, 60)})
        elif filter_deaths == "3":
            f_deaths.extend({Covid.deaths.between(60, 180)})
        elif filter_deaths == "4":
            f_deaths.extend({Covid.deaths.between(180, 540)})
        else:
            f_deaths.extend({Covid.deaths > 540})

    if filter_risk_level != None:
        if filter_risk_level == "n":
            f_risk_level.extend({Covid.risk_level == "None"})
        elif filter_risk_level == "l":
            f_risk_level.extend({Covid.risk_level == "Low"})
        elif filter_risk_level == "m":
            f_risk_level.extend({Covid.risk_level == "Medium"})
        elif filter_risk_level == "h":
            f_risk_level.extend({Covid.risk_level == "High"})
        else:
            f_risk_level.extend({Covid.risk_level == "Severe"})

    if filter_transmission_level != None:
        if filter_transmission_level == "s":
            f_transmission_level.extend({Covid.cdc_transmission_level == "Substantial"})
        elif filter_transmission_level == "l":
            f_transmission_level.extend({Covid.cdc_transmission_level == "Low"})
        elif filter_transmission_level == "m":
            f_transmission_level.extend({Covid.cdc_transmission_level == "Moderate"})
        else:
            f_transmission_level.extend({Covid.cdc_transmission_level == "High"})

    if filter_infection_rate != None:
        if filter_infection_rate == "1":
            f_infection_rate.extend({Covid.infection_rate.between(0, 0.5)})
        elif filter_infection_rate == "2":
            f_infection_rate.extend({Covid.infection_rate.between(0.5, 1)})
        else:
            f_infection_rate.extend({Covid.infection_rate > 1})

    if filter_icu_bed_capacity != None:
        if filter_icu_bed_capacity == "1":
            f_icu_bed_capacity.extend({Covid.icu_bed_capacity.between(0, 10)})
        elif filter_icu_bed_capacity == "2":
            f_icu_bed_capacity.extend({Covid.icu_bed_capacity.between(10, 50)})
        elif filter_icu_bed_capacity == "3":
            f_icu_bed_capacity.extend({Covid.icu_bed_capacity.between(50, 100)})
        elif filter_icu_bed_capacity == "4":
            f_icu_bed_capacity.extend({Covid.icu_bed_capacity.between(100, 500)})
        else:
            f_icu_bed_capacity.extend({Covid.icu_bed_capacity > 500})

    if filter_icu_bed_usage != None:
        if filter_icu_bed_usage == "1":
            f_icu_bed_usage.extend({Covid.icu_bed_current_usage.between(0, 10)})
        elif filter_icu_bed_usage == "2":
            f_icu_bed_usage.extend({Covid.icu_bed_current_usage.between(10, 50)})
        elif filter_icu_bed_usage == "3":
            f_icu_bed_usage.extend({Covid.icu_bed_current_usage.between(50, 100)})
        elif filter_icu_bed_usage == "4":
            f_icu_bed_usage.extend({Covid.icu_bed_current_usage.between(100, 500)})
        else:
            f_icu_bed_usage.extend({Covid.icu_bed_current_usage > 500})

    result = (
        result.filter(or_(*f_cases))
        .filter(or_(*f_deaths))
        .filter(or_(*f_risk_level))
        .filter(or_(*f_transmission_level))
        .filter(or_(*f_infection_rate))
        .filter(or_(*f_icu_bed_capacity))
        .filter(or_(*f_icu_bed_usage))
    )

    sorting_flag = request.args.get("s")
    if sorting_flag != None:
        attribute, order = sorting_flag.split("with")
        if order == "des":
            result = result.order_by(desc(getattr(Covid, attribute)))
            result = result.filter(getattr(Covid, attribute) != None)
        else:
            result = result.order_by(asc(getattr(Covid, attribute)))
            result = result.filter(or_(getattr(Covid, attribute) != None)).filter(
                or_(getattr(Covid, attribute) != "")
            )

    count = len(result.all())
    page = request.args.get("page", type=int)
    page_size = request.args.get("pagesize", type=int)
    if page != None:
        result = result.paginate(page=page, per_page=page_size).items
    else:
        result = result.all()

    return jsonify({"result": covids_schema.dump(result), "count": count})


# get single covid stat by county_name
@app.route("/api/covid/county_name=<county_name>", methods=["GET"])
def get_covid_by_name(county_name):
    covid_stat = Covid.query.filter(
        Covid.county_name == county_name + " County"
    ).one_or_none()
    covid_stat_res = covid_schema.dump(covid_stat)

    city_info = Us_cities.query.filter(Us_cities.COUNTY == county_name)

    city_result = us_cities_schema.dump(city_info)
    city_array = []
    for record in city_result:
        city_array.append(record["CITY"])

    print(city_array)
    covid_stat_res["cities"] = city_array
    return jsonify(covid_stat_res)


# get all flight stats
@app.route("/api/flight", methods=["GET"])
def flight():
    result = db.session.query(Flight_combined)
    searching_flag = request.args.get("f")
    if searching_flag != None:
        search = []
        searching_words = searching_flag.split(" ")
        for word in searching_words:
            search.append(Flight_combined.departure_airport.contains(word))
            search.append(Flight_combined.departure_icao.contains(word))
            search.append(Flight_combined.departure_scheduled.contains(word))
            search.append(Flight_combined.departure_terminal.contains(word))
            search.append(Flight_combined.departure_gate.contains(word))
            search.append(Flight_combined.arrival_airport.contains(word))
            search.append(Flight_combined.arrival_icao.contains(word))
            search.append(Flight_combined.arrival_scheduled.contains(word))
            search.append(Flight_combined.arrival_terminal.contains(word))
            search.append(Flight_combined.arrival_gate.contains(word))
            search.append(Flight_combined.arrival_baggage.contains(word))
            search.append(Flight_combined.airline_name.contains(word))
            search.append(Flight_combined.flight_number.contains(word))
            search.append(Flight_combined.codeshared_name.contains(word))
            search.append(Flight_combined.codeshared_number.contains(word))
            search.append(Flight_combined.city.contains(word))
            search.append(Flight_combined.county.contains(word))

        result = result.filter(or_(*(search)))

    f_departure_icao = []
    f_arrival_scheduled = []
    f_departure_scheduled = []
    f_arrival_icao = []
    f_airline_name = []

    filter_departure_icao = request.args.get("di")
    filter_departure_scheduled_min = request.args.get("dsmin")
    filter_departure_scheduled_max = request.args.get("dsmax")
    filter_arrival_icao = request.args.get("ai")
    filter_arrival_scheduled_min = request.args.get("asmin")
    filter_arrival_scheduled_max = request.args.get("asmax")
    filter_airline_name = request.args.get("an")
    if filter_departure_icao != None:
        f_departure_icao.extend(
            {Flight_combined.departure_icao.contains(filter_departure_icao)}
        )
    if filter_arrival_scheduled_min != None or filter_arrival_scheduled_max != None:
        start, end = filter_arrival_scheduled_min, filter_arrival_scheduled_max
        if start != None and end != None:
            f_arrival_scheduled.extend(
                {Flight_combined.arrival_scheduled.between(start, end)}
            )
        elif end == None:
            f_arrival_scheduled.extend({Flight_combined.arrival_scheduled >= start})
        else:
            f_arrival_scheduled.extend({Flight_combined.arrival_scheduled <= end})
    if filter_departure_scheduled_min != None or filter_departure_scheduled_max != None:
        start, end = filter_departure_scheduled_min, filter_departure_scheduled_max
        if start != None and end != None:
            f_departure_scheduled.extend(
                {Flight_combined.departure_scheduled.between(start, end)}
            )
        elif end == None:
            f_departure_scheduled.extend({Flight_combined.departure_scheduled >= start})
        else:
            f_departure_scheduled.extend({Flight_combined.departure_scheduled <= end})
    if filter_arrival_icao != None:
        f_arrival_icao.extend(
            {Flight_combined.arrival_icao.contains(filter_arrival_icao)}
        )
    if filter_airline_name != None:
        f_airline_name.extend(
            {Flight_combined.airline_name.contains(filter_airline_name)}
        )

    result = (
        result.filter(or_(*f_departure_icao))
        .filter(or_(*f_arrival_scheduled))
        .filter(or_(*f_departure_scheduled))
        .filter(or_(*f_arrival_icao))
        .filter(or_(*f_airline_name))
    )

    sorting_flag = request.args.get("s")
    if sorting_flag != None:
        attribute, order = sorting_flag.split("with")
        print(attribute)
        if order == "des":
            result = result.order_by(desc(getattr(Flight_combined, attribute)))
        else:
            result = result.order_by(asc(getattr(Flight_combined, attribute)))

    count = len(result.all())
    page = request.args.get("page", type=int)
    page_size = request.args.get("pagesize", type=int)
    if page != None:
        result = result.paginate(page=page, per_page=page_size).items
    else:
        result = result.all()

    return jsonify({"result": flights_schema.dump(result), "count": count})


@app.route("/api/flight/info", methods=["GET"])
def get_distinct_info():
    query_departure_icaos = db.session.query(
        Flight_combined.departure_icao.distinct().label("departure_icao")
    )
    query_arrival_icaos = db.session.query(
        Flight_combined.arrival_icao.distinct().label("arrival_icao")
    )
    query_airline_names = db.session.query(
        Flight_combined.airline_name.distinct().label("airline_name")
    )
    departure_icaos = [row.departure_icao for row in query_departure_icaos.all()]
    arrival_icaos = [row.arrival_icao for row in query_arrival_icaos.all()]
    airline_names = [row.airline_name for row in query_airline_names.all()]
    return jsonify(
        {
            "departure_icao": departure_icaos,
            "arrival_icao": arrival_icaos,
            "airline_name": airline_names,
        }
    )


# get single flight stat by id
@app.route("/api/flight/id=<id>", methods=["GET"])
def get_flight_by_id(id):
    flight_stat = Flight_combined.query.get(id)
    return flight_schema.jsonify(flight_stat)


# get single flight stat by city_name
@app.route("/api/flight/city=<city>", methods=["GET"])
def get_flight_by_city(city):
    flight_stat = Flight_combined.query.filter(Flight_combined.city == city)
    return flights_schema.jsonify(flight_stat)


# get all city stats
@app.route("/api/city", methods=["GET"])
def city():
    result = db.session.query(City)
    searching_flag = request.args.get("f")
    if searching_flag != None:
        search = []
        searching_words = searching_flag.split(" ")
        for word in searching_words:
            search.append(City.city_name.contains(word))
            search.append(City.population.contains(word))
            search.append(City.Longitude.contains(word))
            search.append(City.Latitude.contains(word))
            search.append(City.County_name.contains(word))
            search.append(City.sale_tax.contains(word))
            search.append(City.size.contains(word))
            search.append(City.wiki_history.contains(word))
            search.append(City.average_temp.contains(word))
            search.append(City.position.contains(word))

        result = result.filter(or_(*(search)))

    f_sale_tax = []
    f_city_name = []
    f_County_name = []
    f_population = []
    f_size = []

    filter_sale_tax = request.args.get("st")
    filter_city_name = request.args.get("cn")
    filter_County_name = request.args.get("con")
    filter_population = request.args.get("p")
    filter_size = request.args.get("size")
    if filter_sale_tax != None:
        if filter_sale_tax == "1":
            f_sale_tax.extend({City.sale_tax.between(6, 6.5)})
        elif filter_sale_tax == "2":
            f_sale_tax.extend({City.sale_tax.between(6.5, 7)})
        elif filter_sale_tax == "3":
            f_sale_tax.extend({City.sale_tax.between(6.5, 7)})
        elif filter_sale_tax == "4":
            f_sale_tax.extend({City.sale_tax.between(7.5, 8)})
        else:
            f_sale_tax.extend({City.sale_tax > 8})

    if filter_city_name != None:
        f_city_name.extend({City.city_name.startswith(filter_city_name.upper())})

    if filter_County_name != None:
        f_County_name.extend({City.County_name.startswith(filter_County_name.upper())})

    if filter_population != None:
        if filter_population == "1":
            f_population.extend({City.population.between(0, 10000)})
        elif filter_population == "2":
            f_population.extend({City.population.between(10000, 100000)})
        else:
            f_population.extend({City.population > 100000})
    if filter_size != None:
        if filter_size == "1":
            f_size.extend({City.size.between(0, 5)})
        elif filter_population == "2":
            f_size.extend({City.size.between(5, 10)})
        else:
            f_size.extend({City.size > 10})

    result = (
        result.filter(or_(*f_sale_tax))
        .filter(or_(*f_city_name))
        .filter(or_(*f_County_name))
        .filter(or_(*f_population))
        .filter(or_(*f_size))
    )

    sorting_flag = request.args.get("s")
    if sorting_flag != None:
        attribute, order = sorting_flag.split("with")
        print(attribute)
        if order == "des":
            result = result.order_by(desc(getattr(City, attribute)))
            result = result.filter(getattr(City, attribute) != "")
        else:
            result = result.order_by(asc(getattr(City, attribute)))
            result = result.filter(getattr(City, attribute) != "")

    count = len(result.all())
    page = request.args.get("page", type=int)
    page_size = request.args.get("pagesize", type=int)
    if page != None:
        result = result.paginate(page=page, per_page=page_size).items
    else:
        result = result.all()

    return jsonify({"result": cities_schema.dump(result), "count": count})


# get single city stat by city name
@app.route("/api/city/city_name=<city_name>", methods=["GET"])
def get_city_by_name(city_name):
    city_stat = City.query.filter(City.city_name == city_name).one_or_none()
    return city_schema.jsonify(city_stat)


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000, debug=True)
