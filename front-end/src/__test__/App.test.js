import App from '../App.js'
import React from "react"
import { configure, shallow } from "enzyme"
import Adapter from "enzyme-adapter-react-16"
import NavigationBar from '../components/navigationbar/NavigationBar.js'
import Objectshower from '../components/object_shower/Objectshower.js'
import Testpic from '../static_resources/Texas_twitter_follow.jpg'
import Flight from "../components/pages/flight/Flight"
configure({ adapter: new Adapter() });
it('App Page', async () => {
    const copy = shallow(<App />)
    expect(copy).toMatchSnapshot()
})

describe("Test components,", () => {
	test("Navigationbar", () => {
		const navigationBar = shallow(
			<NavigationBar />
		)
		expect(navigationBar).toMatchSnapshot()
	});
  test("object_shower", () => {
    const objs = shallow(
      <Objectshower
        description="Testing"
        url= {Testpic}
        title="Testing1 for object_shower"
        target = "/"
      />
    )
  });
  
});
