import CityModule from '../components/pages/city/cityModule.js'
import React from "react"
import { configure, shallow } from "enzyme"
import Adapter from "enzyme-adapter-react-16"
import CitySearch from "../components/pages/city/CitySearch.js"
import CitySeachCard from "../components/pages/city/CityCard.js"
import { useQueryParam, NumberParam, QueryParamProvider } from 'use-query-params';
configure({ adapter: new Adapter() });
describe("Test city module page" , () =>{
  test('City Module Page exist',() => {
      const ccity = shallow(<QueryParamProvider><CityModule /></ QueryParamProvider>)
      expect(ccity).not.toBeNull()
  });
  test('City Module Page defined', () => {
      const ccity = shallow(<QueryParamProvider><CityModule /></ QueryParamProvider>)
      expect(ccity).toBeDefined()
  });
  test('CityModule page', async () => {
      const copy = shallow(<QueryParamProvider><CityModule /></ QueryParamProvider>)
      expect(copy).toMatchSnapshot()
  });
});

describe("Test City search page", () =>{
  test('City Search Page Testing', () => {
    const cSearch = shallow(<CitySearch />)
    expect(cSearch).toMatchSnapshot()
  });
});

describe("Test City search card", () =>{
  test('City search card testing', () => {
    const testInput = []
    const cardC = shallow(<CitySeachCard
             cityData={testInput}
             searchTerm="Austin"
       />)
    expect(cardC).toMatchSnapshot()
  });
});
