import Flight from '../components/pages/flight/Flight.js'
import React from "react"
import { configure, shallow } from "enzyme"
import Adapter from "enzyme-adapter-react-16"
import { Router } from 'react-router';
import { Route } from 'react-router-dom';
import FlightCard from '../components/pages/flight/FlightCard.js'
import FlightCardHighlight from '../components/pages/flight/FlightCardHighlight.js'
import { useQueryParam, NumberParam, QueryParamProvider } from 'use-query-params';
configure({ adapter: new Adapter() });
describe("Test Flight module page" , () =>{
  test('Flight Page exist',() => {
      const ccity = shallow(<QueryParamProvider><Flight />< / QueryParamProvider>)
      expect(ccity).not.toBeNull()
  });
  test('Flight Page defined', () => {
      const ccity = shallow(<QueryParamProvider><Flight />< / QueryParamProvider>)
      expect(ccity).toBeDefined()
  });
  test('Flight page', async () => {
      const copy = shallow(<QueryParamProvider><Flight />< / QueryParamProvider>)
      expect(copy).toMatchSnapshot()
  });
});

describe("Render views", () => {
	test("Flight", () => {
		const flightTest = shallow(<QueryParamProvider><Flight />< / QueryParamProvider>)
		expect(flightTest).toMatchSnapshot()
	})
});

describe("Test Flight card", () =>{
  test('Flight Card test', () => {
    const testInput = []
    const cardC = shallow(<FlightCard
             flightData={testInput}
       />)
    expect(cardC).toMatchSnapshot()
  });
});

describe("Test Flight card with highlight function", () =>{
  test('Flight card with highlight test (searching)', () => {
    const testInput = []
    const cardCH = shallow(<FlightCardHighlight
             flightData={testInput}
             searchTerm="Austin"
       />)
    expect(cardCH).toMatchSnapshot()
  });
});
