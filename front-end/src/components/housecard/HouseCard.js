import React from "react";
import "./HouseCard.css"

//TODO: Let src accepting image links passed in by url
const HouseCard = ({county, guest, bedroom, bed, bath, price, url, title, target}) => {
    return (
        <div id="houseCards" class="card h-100" style={{width: '18rem' }}>
            <img class="card-img-top" src={url} alt="Image failed to load" />
            <div class="card-body">
                <h5 class="card-title">{title}</h5>
                <p class="card-text">County: {county}</p>
                <p class="card-text">Guests: {guest}</p>
                <p class="card-text">Bedrooms: {bedroom}</p>
                <p class="card-text">Beds: {bed}</p>
                <p class="card-text">Baths: {bath}</p>
                <p class="card-text">Price: {price}$/night</p>
                <a href={target} class="stretched-link"> </a>
            </div>
        </div>
    )
}

export default HouseCard;
