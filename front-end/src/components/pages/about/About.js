import React, { useEffect, useState } from 'react'
import axios from "axios"
import { Card, Container, ListGroup, ListGroupItem, Row, Col } from 'react-bootstrap';
import bowen_photo from '../../../static_resources/bowen_photo.jpeg';
import eric_photo from '../../../static_resources/eric_photo.jpg';
import kelly_photo from '../../../static_resources/kelly_photo.png';
import yuhan_photo from '../../../static_resources/yuhan_photo.jpeg';

import react_logo from '../../../static_resources/react_logo.png';
import discord_logo from '../../../static_resources/discord_logo.png';
import docker_logo from '../../../static_resources/docker_logo.png';
import aws_logo from '../../../static_resources/aws_logo.png';
import gitlab_logo from '../../../static_resources/gitlab_logo.png';

import aviationstack_logo from '../../../static_resources/aviationstack_logo.jpg';
import covid_act_now_logo from '../../../static_resources/covid_act_now_logo.png';
import sale_tax_logo from '../../../static_resources/sale_tax_logo.png';
import postman_logo2 from '../../../static_resources/postman_logo2.png';
import wikipedia_logo from '../../../static_resources/wikipedia_logo.png';
import usa_com_logo from '../../../static_resources/usa_com_logo.png';
import arcgis_logo from '../../../static_resources/arcgis_logo.png';
import flightaware_logo from '../../../static_resources/flightaware_logo.png';

import './About.css';

class UserInfo {
    constructor (name, filterusername, username, image, bio, unittests, role) {
        this.name = name;
        this.filterusername = filterusername;
        this.username = username;
        this.commits = 0;
        this.issues = 0;
        this.image = image;
        this.bio = bio;
        this.unittests = unittests;
        this.role = role;
    }
}

const baseUserInfo = [
    new UserInfo(
        "Yuhan Gao",
        "Yuhan Gao",
        "yhgaoFAGBAT",
        yuhan_photo,
        "I am a senior CS major at UT Austin. I grew up in Changchun, China. I play a lot of instruments (western and eastern).",
        36,
        "Backend, Stage #2 Lead"
    ), 
    new UserInfo(
        "Eric Chen",
        "Eric L Chen",
        "ericlchen",
        eric_photo,
        "I'm a third year CS major at UT Austin. I grew up in Plano, Texas. I'm part of the UT Austin Fencing and enjoy watching Youtube.",
        24,
        "Frontend, Stage #1 Lead"
    ),
    new UserInfo(
        "Bowen Yang",
        "BWN133",
        "BWN133",
        bowen_photo,
        "I am a CS major spring term Junior and I am interested in robotics, A.R., and A.I. I grew up in Shenzhen China, and I love music and can play piano and guitar.",
        20,
        "Frontend, Stage #3 Lead"
    ),
    new UserInfo(
        "Kelly Flores",
        "Kelly Flores",
        "kellyflores",
        kelly_photo,
        "I'm a fourth year Computer Science major at UT Austin with a minor in business . I grew up in Eagle Pass, Texas. I love playing and watching football soccer, so much so that I am a 3 year champ in my fantasy league.",
        0,
        "Frontend, Stage #4 Lead"
    )
];

class InfoCard {
    constructor (name, bio, image, link) {
        this.name = name;
        this.bio = bio;
        this.image = image;
        this.link = link;
    }
}

let baseApiInfo = [
    new InfoCard(
        "Aviationstack",
        "Used to find information about flights",
        aviationstack_logo,
        "https://aviationstack.com/"
    ), 
    new InfoCard(
        "Flightaware",
        "Used to find information about airports",
        flightaware_logo,
        "https://flightaware.com/commercial/aeroapi/"
    ), 
    new InfoCard(
        "ArcGIS Hub",
        "Used to find coordinate data on Texas cities",
        arcgis_logo,
        "https://hub.arcgis.com/datasets/TXDOT::texas-cities/explore?location=31.199256%2C-100.111598%2C6.71"
    ), 
    new InfoCard(
        "USA.com",
        "Used to find land area data on Texas cities",
        usa_com_logo,
        "http://www.usa.com/rank/texas-state--land-area--city-rank.htm"
    ), 
    new InfoCard(
        "Sale-Tax",
        "Used to find sales tax in Texas cities",
        sale_tax_logo,
        "https://www.sale-tax.com/Texas_all"
    ),
    new InfoCard(
        "Wikipedia",
        "Used to find descriptions on Texas cities",
        wikipedia_logo,
        "https://en.wikipedia.org/wiki/Main_Page"
    ),
    new InfoCard(
        "Covid Act Now",
        "Used to find Covid-19 information",
        covid_act_now_logo,
        "https://covidactnow.org/data-api"
    ),
];

let baseToolInfo = [
    new InfoCard(
        "React",
        "Front-end JavaScript library for building user interfaces",
        react_logo,
        "https://reactjs.org/"
    ),
    new InfoCard(
        "Postman",
        "API platform for building and using APIs",
        postman_logo2,
        "https://www.postman.com/"
    ),
    new InfoCard(
        "AWS",
        "Cloud computing platforms and APIs",
        aws_logo,
        "https://aws.amazon.com/"
    ),
    new InfoCard(
        "Docker",
        "Containerization platform used to standardize execution",
        docker_logo,
        "https://www.docker.com/"
    ),
    new InfoCard(
        "Gitlab",
        "DevOps lifecycle tool that provides a Git repository manager",
        gitlab_logo,
        "https://about.gitlab.com/"
    ),
    new InfoCard(
        "Discord",
        "Team messaging platform",
        discord_logo,
        "https://discord.com/"
    ),
]

function About () {
    const [ baseUserData ] = useState(baseUserInfo);
    const [ commitData, setCommitData ] = useState(0);
    const [ issueData, setIssueData ] = useState(0);
    const [ unitData ] = useState(60);
    
    useEffect(() => {
        const getCommits = async () => {
            await axios.get("https://gitlab.com/api/v4/projects/29889115/repository/contributors")
                .then((response) => response.data)
                .then((data) => {
                    let commits = 0;
                    data.forEach((user) => {
                        const contributor = baseUserData.find((con) => con.filterusername === user.name || con.username === user.name);
                        if (contributor !== undefined) {
                            contributor.commits += user.commits;
                            commits += user.commits;
                        }
                    });
                    setCommitData(commits);
                });
        }
        getCommits();
    }, []);

    useEffect(() => {
        const getIssues = async () => {
            let issues = 0;
            Promise.all(baseUserData.map(async (user) => {
                await axios.get("https://gitlab.com/api/v4/projects/29889115/issues_statistics?author_username=" + user.username)
                    .then((response) => response.data)
                    .then((data) => {
                        user.issues = data.statistics.counts.all;
                        issues += user.issues;
                    });
            })).then(() => {
                setIssueData(issues);
            });
        }
        getIssues();
    }, []);

    return (
        <div style={{ background: 'gainsboro'}}>
            <Container>
            <div>
                <h1>About Us</h1>
                <p style={{ fontSize: '25px' }}>
                This website is to help customers who decide to travel to Texas safely. 
                The customers can choose flights to Texas. 
                We provide the basic information about Texas cities such as the population, city size, sales tax, and history.
                There is also Covid information on each county.
                </p>
            </div>
            <br/>
            <div>
                <h1>Project Information</h1>
                <Row id="hoverable">
                    <Col sm={6} style={{marginBottom: "10px"}}>
                        <Card style={{height:'100%', width: '100%'}}>
                            <Card.Img variant="top" src={gitlab_logo} style={{objectFit: 'scale-down', height: '20vh'}}/>
                            <a href="https://gitlab.com/BWN133/atraveltexas" class="stretched-link"> </a>
                        </Card>
                    </Col>
                    <Col sm={6} style={{marginBottom: "10px"}}>
                        <Card style={{height:'100%', width: '100%'}}>
                            <Card.Img variant="top" src={postman_logo2} style={{objectFit: 'scale-down', height: '20vh'}}/>
                            <a href="https://documenter.getpostman.com/view/13891407/UUy37RVh" class="stretched-link"> </a>
                        </Card>
                    </Col>
                </Row>
                <Row style={{display: 'flex', flexDirection: 'row', justifyContent: 'center' }}>
                    <Col sm={4}>
                    <Card style={{ margin: '10px'}}>
                        <Card.Body>
                            <Card.Text>Total commits: {commitData}</Card.Text>
                        </Card.Body>
                    </Card>
                    </Col>
                    <Col sm={4}>
                    <Card style={{ margin: '10px'}}>
                        <Card.Body>
                            <Card.Text>Total issues: {issueData}</Card.Text>
                        </Card.Body>
                    </Card>
                    </Col>
                    <Col sm={4}>
                    <Card style={{ margin: '10px'}}>
                        <Card.Body>
                            <Card.Text>Total unit tests: {unitData}</Card.Text>
                        </Card.Body>
                    </Card>
                    </Col>
                </Row>
            </div>
            <br/>
            <div>
                <h1>Our Team</h1>
                <Row style={{display:'flex'}}>
                    {baseUserData.map((data) => (
                        <Col sm={3} style={{marginBottom: "10px"}}>
                        <Card style={{height:'100%', width: '100%'}}>
                            <Card.Img variant="top" src={data.image} style={{objectFit: 'cover', height: '30vh'}}/>
                            <Card.Body>
                                <Card.Title>{data.name}</Card.Title>
                                <Card.Text>
                                {data.bio}
                                </Card.Text>
                            </Card.Body>
                            <ListGroup className="list-group-flush">
                                <ListGroupItem>Role: {data.role}</ListGroupItem>
                                <ListGroupItem>Commits: {data.commits}</ListGroupItem>
                                <ListGroupItem>Issues: {data.issues}</ListGroupItem>
                                <ListGroupItem>Unit tests: {data.unittests}</ListGroupItem>
                            </ListGroup>
                        </Card>
                        </Col>
                    ))}
                </Row>
            </div>
            <br/>
            <div id="hoverable">
                <h1>APIs</h1>
                <Row>
                    {baseApiInfo.map((data) => (
                        <Col sm={4} style={{marginBottom: "10px"}}>
                            <Card style={{height:'100%', width: '100%'}}>
                                <Card.Img variant="top" src={data.image} style={{objectFit: 'scale-down', height: '20vh'}}/>
                                <Card.Body>
                                    <Card.Title>{data.name}</Card.Title>
                                    <Card.Text>
                                    {data.bio}
                                    </Card.Text>
                                </Card.Body>
                                <a href={data.link} class="stretched-link"> </a>
                            </Card>
                        </Col>
                    ))}
                </Row>
            </div>
            <br/>
            <div id="hoverable">
                <h1>Tools</h1>
                <Row>
                    {baseToolInfo.map((data) => (
                        <Col sm={4} style={{marginBottom: "10px"}}>
                            <Card style={{height:'100%', width: '100%'}}>
                                <Card.Img variant="top" src={data.image} style={{objectFit: 'scale-down', height: '20vh'}}/>
                                <Card.Body>
                                    <Card.Title>{data.name}</Card.Title>
                                    <Card.Text>
                                    {data.bio}
                                    </Card.Text>
                                </Card.Body>
                                <a href={data.link} class="stretched-link"> </a>
                            </Card>
                        </Col>
                    ))}
                </Row>
            </div>
            <br/>
            </Container>
        </div>
    );
}

export default About;