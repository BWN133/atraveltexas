import React from 'react'
import Highlighter from 'react-highlight-words';
import { Card, Col, ListGroup, ListGroupItem, Row } from 'react-bootstrap';

function CityCard ({ cityData, searchTerm }){
  console.log("cityData here!!!!!!");
  console.log(cityData);
  let searchWords = searchTerm.split(" ");
  if (cityData?.length !== 0)
  return (
    <Row id="hoverable">
        {cityData.map(data => (
            <Col sm={3} style={{marginBottom: '10px'}}>
                <Card style={{ height: '100%', width: '100%' }}>
                    <Card.Body>
                        <Card.Title>
                            <Highlighter
                                searchWords={searchWords}
                                textToHighlight={`${data?.city_name}`}
                            />
                        </Card.Title>
                    </Card.Body>
                    <ListGroup className="list-group-flush">
                        <ListGroupItem><b>County Name: </b>
                            <Highlighter
                                searchWords={searchWords}
                                textToHighlight={data?.County_name ?
                                    ' '+data.County_name : ' No Data'}
                            />
                        </ListGroupItem>
                        <ListGroupItem><b>Sale Tax (%): </b>
                            <Highlighter
                                searchWords={searchWords}
                                textToHighlight={data?.sale_tax ?
                                    ' '+data.sale_tax : ' Unknown'}
                            />
                        </ListGroupItem>
                        <ListGroupItem><b>Size  (mi²): </b>
                            <Highlighter
                                searchWords={searchWords}
                                textToHighlight={data?.size ?
                                    ' '+data.size : ' Unknown'}
                            />
                        </ListGroupItem>
                        <ListGroupItem><b>Population: </b>
                            <Highlighter
                                searchWords={searchWords}
                                textToHighlight={data?.population ?
                                    ' '+data.population : ' Unknown'}
                            />
                        </ListGroupItem>
                        <ListGroupItem><b>Average Temperature (°C): </b>
                            <Highlighter
                                searchWords={searchWords}
                                textToHighlight={data?.average_temp?
                                    ' '+data.average_temp : ' Unknown'}
                            />
                        </ListGroupItem>
                    </ListGroup>
                    <a href={"/cityModule/"+data.city_name} class="stretched-link"> </a>
                </Card>
            </Col>
        ))}
    </Row>
  );
  return (<div>No results found.</div>);
}


export default CityCard;
