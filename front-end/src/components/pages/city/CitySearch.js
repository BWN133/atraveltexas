import { Container, Carousel, DropdownButton, Dropdown, ButtonGroup, Button, Form, FormControl, Spinner,Row, Col} from 'react-bootstrap';
import { useHistory } from "react-router-dom";
import React, { useEffect, useState } from 'react';
import axios from "axios";
import ReactPaginate from 'react-paginate';
import "./city.css"
import CityCard from './CityCard';

function CitySearch(){
  const [searchState, setState] = useState({val: ""});
  const [ cityData, setCityData] = useState({});
  const [data, setData] = useState([]);
  const [totalSize, setSize] = useState(0);
  const history = useHistory();
  const [isLoading, setIsLoading] = useState(true);
  const pageCount = Math.ceil(totalSize/10);
  const [urlParam, setUrlParam] = useState(
    {
      page:1,
      pagesize:10,
    }
  );

  const onSubmit = () => {
    console.log(searchState.val);
    setUrlParam({
      ...urlParam,
      f: searchState.val
    });
  }

  useEffect(() => {
      const getData = async () => {
          setIsLoading(true);
          const response = await axios.get("https://api.atraveltx.me/api/city", {params:urlParam});
          setSize(response.data.count);
          setData(response.data.result);
          setIsLoading(false);
      }
      getData();
  }, [urlParam]);
  const handlePageClick = (data) => {
      setUrlParam({
          ...urlParam,
          page: data.selected + 1
      });
  }
  return(
    <div id="filterBorder">
      <h1> City Seach </ h1>
      <div>
        <input type="text" size = "sm" placeholder="Search" className="center" id="searchBar" onChange={e => setState({ val: e.target.value })}/>
        <button title="search" id="searchButton" onClick={onSubmit}>Search City</button>
      </ div>
      {isLoading ? (
          <div class="d-flex justify-content-center">
              <Spinner animation="border" role="status">
                  <span className="visually-hidden">Loading...</span>
              </Spinner>
          </div>
      ) : (
          urlParam?.f ?
          (<div>
            <CityCard
              cityData={data}
              searchTerm={urlParam.f}
          />
          <div style={{width:'100%'}} align='right'>
                    <ReactPaginate
                        previousLabel={'previous'}
                        nextLabel={'next'}
                        breakLabel={'...'}
                        pageCount={pageCount}
                        forcePage={urlParam.page - 1}
                        marginPagesDisplayed={1}
                        pageRangeDisplayed={3}
                        onPageChange={handlePageClick}
                        subContainerClassName={'pages pagination'}
                        breakClassName={'page-item'}
                        breakLinkClassName={'page-link'}
                        containerClassName={'pagination'}
                        pageClassName={'page-item'}
                        pageLinkClassName={'page-link'}
                        previousClassName={'page-item'}
                        previousLinkClassName={'page-link'}
                        nextClassName={'page-item'}
                        nextLinkClassName={'page-link'}
                        activeClassName={'active'}
                    />
                </div>
              </ div>) :
          <div> </ div>
      )}
      <div style={{display:'flex', position:"float", justifyContent:"center", margin_bottom:"50px"}} className = "margin_up">
          <div style={{margin_right: '50px', marginLeft: '50px'}}>
          </ div>
      </div>
    </ div>
  );

}

export default CitySearch;
