import React from 'react'
import Objectshower from '../../object_shower/Objectshower';
import "../home/Home.css"
import "./city.css"
import { Button, Card, CardDeck, Col, Container, Row } from "react-bootstrap"
import Travis_county from '../../../static_resources/Travis_county.jpg'
import Iframe from 'react-iframe'
import { useEffect, useState } from 'react'
import { useParams } from 'react-router';
import { useHistory } from "react-router-dom";
import axios from "axios"
import BootstrapTable from "react-bootstrap-table-next";
import filterFactory, {textFilter} from "react-bootstrap-table2-filter";
import paginationFactory, {PaginationProvider} from 'react-bootstrap-table2-paginator';
import 'react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css';
import 'react-bootstrap-table2-filter/dist/react-bootstrap-table2-filter.min.css';
import Texas_twitter from '../../../static_resources/Texas_twitter_follow.jpg';
import { GoogleMap, withScriptjs, withGoogleMap, Marker } from 'react-google-maps';
import ReactTextCollapse from "react-text-collapse";
const columnHeaders = [{
    dataField: 'departure_airport',
    text: 'Departure Airport',
    sort: true
}, {
    dataField: 'arrival_airport',
    text: 'Arrival airport',
    sort: true
}, {
    dataField: 'airline_name',
    text: 'Airline',
    sort: true
},
];

function City() {
    const city_name = useParams().city_name;
    const [cityData, getCityData] = useState([]);
    const [flightData, getFlightData] = useState([]);
    useEffect(() => {
    const getData = async () => {
            const responseCity = await axios.get("https://api.atraveltx.me/api/city/city_name="+city_name);
            const responseFlight = await axios.get("https://api.atraveltx.me/api/flight/city="+city_name);
            getCityData(responseCity.data);
            getFlightData(responseFlight.data);
        }
        getData();
    }, []);

    const history = useHistory();
    const tableRowEvents = {
        onClick: (e, row, rowIndex) => {
            history.push("/flight/" + row.id);
        }
    }
    const TEXT_COLLAPSE_OPTIONS = {
      collapse: false,
      collapseText: '... show more',
      expandText: 'show less',
      minHeight: 215,
      maxHeight: 300,
      textStyle: { // pass the css for the collapseText and expandText here
        color: "blue",
        fontSize: "20px"
      }
    }

    const basicData = [cityData?.population? cityData.population: 0, cityData.size, cityData.sale_tax];
    const basicParam = ["Population", "Size", "Sale Tax"];
    // console.log(typeof(cityData.County_name));
    var url_to_covid = "";
    var has_county = true;
    if (cityData.County_name) {
        // console.log(cityData.County_name);
        url_to_covid=cityData.County_name.slice(0, cityData.County_name.length -7);
        // console.log(url_to_covid);
        url_to_covid="/Covid/" + url_to_covid;
    }else{
      has_county = false;
    }
    const gotAD = (flightData.length != 0);
    const gotH = (cityData.wiki_history != null && cityData.wiki_history.length != 0);
    console.log(gotH);
    return (
        <div className="mainPage">
            <Container>
            <h2 className="text-center"> {cityData.city_name}</h2>
            <div class="iframe_wrapper">
                <Iframe class="google_map" height="100%" width="100%" allowfullscreen url={"https://www.google.com/maps/embed/v1/view?zoom=10&center=" + cityData.Latitude + "%2C" + cityData.Longitude + "&key=AIzaSyA9uRD4nAJYAuglAT8bvbpXmamug0Is_1o"}/>
            </div>
            <br/>

            <Row>
                <Col md={2}/>
                <Col md={4}>
                <Card style={{height: '100%'}}>
                    <Card.Header>
                        <h4 className="display-8 text-center mb-4">City Information</h4>
                    </Card.Header>
                    <Card.Body>
                        <table class="table table-condensed table-borderless table-striped">
                            <tbody>
                                <tr>
                                <th scope="row">Longitude</th>
                                <td>{cityData.Longitude}</td>
                                </tr>
                                <tr>
                                <th scope="row">Latitude</th>
                                <td>{cityData.Latitude}</td>
                                </tr>
                                <tr>
                                <th scope="row">County</th>
                                <td>{cityData.County_name ? cityData.County_name : 'no data'}</td>
                                </tr>
                                <tr>
                                <th scope="row">Population</th>
                                <td>{cityData.population ? cityData.population : 'no data'}</td>
                                </tr>
                                <tr>
                                <th scope="row">Size</th>
                                <td>{cityData.size}</td>
                                </tr>
                                <tr>
                                <th scope="row">Sale Tax</th>
                                <td>{cityData.sale_tax}</td>
                                </tr>
                            </tbody>
                        </table>
                    </Card.Body>
                </Card>
                </Col>
                <Col md={4}>
                    <Objectshower url={Texas_twitter} title="Texas Government Twitter!" target="https://twitter.com/texasgov" />
                </Col>
                <Col md={2}/>
            </Row>

            <br/>
            {gotH? (<ReactTextCollapse options={TEXT_COLLAPSE_OPTIONS}>
              <p>
                {cityData.wiki_history}
              </p>
            </ReactTextCollapse>): (<div></div>)}
            <br />
            {gotAD ? (
              <div>
              <h3>Airline Information</h3>
              <BootstrapTable
                  bootstrap4
                  bordered={ false }
                  hover
                  keyField='id'
                  data={flightData}
                  columns={columnHeaders}
                  pagination={paginationFactory({sizePerPage: 5, showTotal: true})}
                  rowEvents={tableRowEvents}
              />
              </ div>
            ): (<h3> No Flight Data For Current City </ h3>)}

            <br/>

            <div style={{display:'flex', position:"float", justifyContent:"center", margin_bottom:"50px"}}>
                <div style={{margin_right: '50px', marginLeft: '50px'}}>
                <Button variant="primary" class="btn btn-primary" style={{width:"200px"}} href="/cityModule">Back to Cities</Button>
                </ div>
                {has_county ? (<div style={{margin_right: '50px', marginLeft: '50px'}}>
                                <Button variant="primary" class="btn btn-primary" style={{width:"200px"}} href={url_to_covid}>{cityData?.County_name? "Covid Info" : "No Covid data"}</Button>
                                </ div>):(<div> </div>) }
                <div style={{margin_right: '50px', marginLeft: '50px'}}>
                <Button variant="primary" class="btn btn-primary" style={{width:"200px"}} href={cityData.wiki_link}>Go to Wiki</Button>
                </ div>
            </div>
            <div style={{height:"50px"}}/>
            </Container>
        </div>
    );

}

export default City;
