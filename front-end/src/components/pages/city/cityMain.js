import CityModule from "./cityModule"
import CitySearch from "./CitySearch"

import { Container, Carousel, DropdownButton, Dropdown, ButtonGroup, Button, Form, FormControl, Spinner, Tabs, Tab} from 'react-bootstrap';
import { Route, useHistory, useLocation } from "react-router-dom";
import BootstrapTable from "react-bootstrap-table-next";
import filterFactory, {textFilter} from "react-bootstrap-table2-filter";
import paginationFactory, {PaginationProvider} from 'react-bootstrap-table2-paginator';
import 'react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css';
import 'react-bootstrap-table2-filter/dist/react-bootstrap-table2-filter.min.css';
import React, { useEffect, useState } from 'react';
import axios from "axios";
import flight_image from '../../../static_resources/flight_image.jpg'
import covid_image from '../../../static_resources/covid_image.jpg'
import district_image from '../../../static_resources/district_image.jpg'
import {
    useQueryParams,
    StringParam,
    NumberParam,
    ArrayParam,
    withDefault,
  } from 'use-query-params';
const { TabPane } = Tabs



function CityMain(){
  const [currKey, setCurrKey] = useState("cityModule");
  const history = useHistory();
  const location = useLocation();
  const handleChange = (key) => {
		history.push(`${key}`);
		setCurrKey(key);
	}

  return (
    <div className="mainPage">
      <div>
        <Tabs
            activeKey={currKey}
            onSelect={(k) => setCurrKey(k)}
            className="mb-3"
          >
        <Tab eventKey="cityModule" title="View All">
          <CityModule />
        </Tab>
        <Tab eventKey="citySearch" title="Search">
          <CitySearch />
        </Tab>

      </Tabs>
		 </div>
  </ div>
  );

}

export default CityMain;
