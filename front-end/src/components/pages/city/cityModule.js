import { Container, Carousel, DropdownButton, Dropdown, ButtonGroup, Button, Form, FormControl, Spinner} from 'react-bootstrap';
import { useHistory } from "react-router-dom";
import BootstrapTable from "react-bootstrap-table-next";
import filterFactory, {textFilter} from "react-bootstrap-table2-filter";
import paginationFactory, {PaginationProvider} from 'react-bootstrap-table2-paginator';
import 'react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css';
import 'react-bootstrap-table2-filter/dist/react-bootstrap-table2-filter.min.css';
import React, { useEffect, useState } from 'react';
import axios from "axios";
import flight_image from '../../../static_resources/flight_image.jpg'
import covid_image from '../../../static_resources/covid_image.jpg'
import district_image from '../../../static_resources/district_image.jpg'
import {
    useQueryParams,
    StringParam,
    NumberParam,
    ArrayParam,
    withDefault,
  } from 'use-query-params';
//{'County_name': '', 'Have_county': 'N', 'Latitude': 32.76018557, 'Longitude': -100.6920785, 'city_name': 'Camp Springs', 'city_number': '', 'id': 0, 'population': '', 'sale_tax': 'no data', 'size': 'no data', 'wiki_link': 'https://en.wikipedia.org/wiki/Camp_Springs,_Texas'}
const columnHeaders = [{
    dataField: 'city_name',
    text: 'City'
}, {
    dataField: 'population',
    text: 'Population'
}, {
  dataField: 'sale_tax',
    text: 'Sale Tax  (%)'
}, {
    dataField: 'size',
    text: 'Size  (mi²)'
}, {
    dataField: 'County_name',
    text: 'County'
}, {
    dataField: 'position',
    text: 'Position'
}, {
    dataField: 'average_temp',
    text: 'Average Temperature (°C)'
}
];

var count = 0;
function CityModule () {
    const [ cityData, setCityData] = useState({});
    const [data, setData] = useState([]);
    const [totalSize, setSize] = useState(0);
    const [urlParam, setUrlParam] = useState(
      {
        page:1,
        pagesize:10,
      }
    );

    const [searchState, setState] = useState({val: ""});
    const [isLoading, setIsLoading] = useState(true);
    const history = useHistory();

    const tableRowEvents = {
        onClick: (e, row, rowIndex) => {
            history.push("/cityModule/" + row.city_name);
        }
    };
    const [queryParam, setQueryParam] = useQueryParams({
        page: withDefault(NumberParam, 1),
        pagesize: withDefault(NumberParam, 20),
        p: StringParam,
        st: StringParam,
        cn: StringParam,
        con: StringParam,
        size: StringParam,
        f: StringParam
    });
    useEffect(() => {
        const getData = async () => {
          console.log("changing");
            setIsLoading(true);
            const response = await axios.get("https://api.atraveltx.me/api/city", {params:queryParam});
            setSize(response.data.count);
            setData(response.data.result);
            setIsLoading(false);
        }
        getData();
    }, [urlParam]);
    const handlePageClick = (cursizePerPage, curpage) => {
    setQueryParam({
        ...queryParam,
        page: curpage,
        pagesize:cursizePerPage
      });
      setUrlParam({
          ...urlParam,
          page: curpage,
          pagesize:cursizePerPage
        });
    }
    console.log(cityData);
    const paginationOption = {
      showTotal:true,
      totalSize: totalSize,
      paginationSize:5,
      onSizePerPageChange: (sizePerPage, page) => {
      handlePageClick(sizePerPage,page);
      },
      onPageChange: (page, sizePerPage) => {
      handlePageClick(sizePerPage,page);
      }
    };

    const stOnclick = (data, unused) => {
      console.log(data);
      setQueryParam({
        ...queryParam,
        st:data
      });
      setUrlParam({
        ...urlParam,
        st:data
      });
    }
    const pOnclick = (data, unused) => {
      console.log(data);
      setQueryParam({
        ...queryParam,
        p:data
      });
      setUrlParam({
        ...urlParam,
        p:data
      });
    }
    const sizeOnclick = (data, unused) => {
      console.log(data);
      setQueryParam({
        ...queryParam,
        size:data
      });
      setUrlParam({
        ...urlParam,
        size:data
      });
    }
    const cnOnclick = (data, unused) => {
      console.log(data);
      setQueryParam({
        ...queryParam,
        cn:data
      });
      setUrlParam({
        ...urlParam,
        cn:data
      });
    }
    const conOnclick = (data, unused) => {
      console.log(data);
      setQueryParam({
        ...queryParam,
        con:data
      });
      setUrlParam({
        ...urlParam,
        con:data
      });
    }
    const sortOnclick = (data, unused) => {
      console.log(data);
      setQueryParam({
        ...queryParam,
        s:data
      });
      setUrlParam({
        ...urlParam,
        s:data
      });
    }
    const clearOnclick = () => {
      const curPage = urlParam["page"];
      const curPageSize = urlParam["pagesize"];
      console.log(curPage);
      console.log(curPageSize);
      setQueryParam({
        page:curPage,
        pagesize:curPageSize,
        p: undefined,
        st: undefined,
        cn: undefined,
        con: undefined,
        size: undefined,
        f: undefined
      });
      setUrlParam({
        page:curPage,
        pagesize:curPageSize
      });
    };

    const onSubmit = () => {
      console.log(searchState.val);
      setQueryParam({
        ...queryParam,
        f: searchState.val
      });
      setUrlParam({
        ...urlParam,
        f: searchState.val
      });
    }
    return (
        <div>
            <h1>City</h1>
            <div>
            <Carousel>
                <Carousel.Item>
                  <img
                    src={district_image}
                    alt="First slide"
                  />
                  <Carousel.Caption>
                    <h3>Welcome to Texas!!!</h3>
                    <p>We are aiming to help you to stay safe in Texas!</p>
                  </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                  <img
                    src={covid_image}
                    alt="Second slide"
                  />

                  <Carousel.Caption>
                    <h3>Covid</h3>
                    <p>We keep track of covid data in Texas for you</p>
                  </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                  <img
                    src={flight_image}
                    alt="Third slide"
                  />
                  <Carousel.Caption>
                    <h3>Flights</h3>
                    <p>We organized all flight data for you in Texas</p>
                  </Carousel.Caption>
                </Carousel.Item>
              </Carousel>
            </ div>
            <div id="filterBorder" className = "clearfix margin_up">
              <DropdownButton id="dropdown-basic-button" title="Sale Tax" size="sm" className = "filterButtons" onSelect={stOnclick}>
                  <Dropdown.Item eventKey="1">6%-6.5%</Dropdown.Item>
                  <Dropdown.Item eventKey="2">6.5%-7%</Dropdown.Item>
                  <Dropdown.Item eventKey="3">7%-7.5%</Dropdown.Item>
                  <Dropdown.Item eventKey="4">7.5%-8%</Dropdown.Item>
                  <Dropdown.Item eventKey="5">8% or above</Dropdown.Item>
              </DropdownButton>

              <DropdownButton id="dropdown-basic-button" title="Population"  size="sm" className = "filterButtons" onSelect={pOnclick}>
                  <Dropdown.Item eventKey="1">0 -10000</Dropdown.Item>
                  <Dropdown.Item eventKey="2">10000 - 100000</Dropdown.Item>
                  <Dropdown.Item eventKey="3">100000 or above</Dropdown.Item>
              </DropdownButton>
              <DropdownButton id="dropdown-basic-button" title="Size (mi²)"  size="sm" className = "filterButtons" onSelect={sizeOnclick}>
                  <Dropdown.Item eventKey="1">0-5</Dropdown.Item>
                  <Dropdown.Item eventKey="2">5-10</Dropdown.Item>
                  <Dropdown.Item eventKey="3">10 or above</Dropdown.Item>
              </DropdownButton>
              <DropdownButton id="dropdown-basic-button" title="City Name"  size="sm" className = "filterButtons" onSelect={cnOnclick}>
                  <Dropdown.Item eventKey="a">a</Dropdown.Item>
                  <Dropdown.Item eventKey="b">b</Dropdown.Item>
                  <Dropdown.Item eventKey="c">c</Dropdown.Item>
                  <Dropdown.Item eventKey="d">d</Dropdown.Item>
                  <Dropdown.Item eventKey="e">e</Dropdown.Item>
                  <Dropdown.Item eventKey="f">f</Dropdown.Item>
                  <Dropdown.Item eventKey="g">g</Dropdown.Item>
                  <Dropdown.Item eventKey="h">h</Dropdown.Item>
                  <Dropdown.Item eventKey="i">i</Dropdown.Item>
                  <Dropdown.Item eventKey="j">j</Dropdown.Item>
                  <Dropdown.Item eventKey="k">k</Dropdown.Item>
                  <Dropdown.Item eventKey="l">l</Dropdown.Item>
                  <Dropdown.Item eventKey="m">m</Dropdown.Item>
                  <Dropdown.Item eventKey="n">n</Dropdown.Item>
                  <Dropdown.Item eventKey="o">o</Dropdown.Item>
                  <Dropdown.Item eventKey="p">p</Dropdown.Item>
                  <Dropdown.Item eventKey="q">q</Dropdown.Item>
                  <Dropdown.Item eventKey="r">r</Dropdown.Item>
                  <Dropdown.Item eventKey="s">s</Dropdown.Item>
                  <Dropdown.Item eventKey="t">t</Dropdown.Item>
                  <Dropdown.Item eventKey="u">u</Dropdown.Item>
                  <Dropdown.Item eventKey="v">v</Dropdown.Item>
                  <Dropdown.Item eventKey="w">w</Dropdown.Item>
                  <Dropdown.Item eventKey="y">y</Dropdown.Item>
                  <Dropdown.Item eventKey="z">z</Dropdown.Item>
              </DropdownButton>
              <DropdownButton id="dropdown-basic-button" title="County Name"  size="sm" className = "filterButtons" onSelect={conOnclick}>
                  <Dropdown.Item eventKey="a">a</Dropdown.Item>
                  <Dropdown.Item eventKey="b">b</Dropdown.Item>
                  <Dropdown.Item eventKey="c">c</Dropdown.Item>
                  <Dropdown.Item eventKey="d">d</Dropdown.Item>
                  <Dropdown.Item eventKey="e">e</Dropdown.Item>
                  <Dropdown.Item eventKey="f">f</Dropdown.Item>
                  <Dropdown.Item eventKey="g">g</Dropdown.Item>
                  <Dropdown.Item eventKey="h">h</Dropdown.Item>
                  <Dropdown.Item eventKey="i">i</Dropdown.Item>
                  <Dropdown.Item eventKey="j">j</Dropdown.Item>
                  <Dropdown.Item eventKey="k">k</Dropdown.Item>
                  <Dropdown.Item eventKey="l">l</Dropdown.Item>
                  <Dropdown.Item eventKey="m">m</Dropdown.Item>
                  <Dropdown.Item eventKey="n">n</Dropdown.Item>
                  <Dropdown.Item eventKey="o">o</Dropdown.Item>
                  <Dropdown.Item eventKey="p">p</Dropdown.Item>
                  <Dropdown.Item eventKey="q">q</Dropdown.Item>
                  <Dropdown.Item eventKey="r">r</Dropdown.Item>
                  <Dropdown.Item eventKey="s">s</Dropdown.Item>
                  <Dropdown.Item eventKey="t">t</Dropdown.Item>
                  <Dropdown.Item eventKey="u">u</Dropdown.Item>
                  <Dropdown.Item eventKey="v">v</Dropdown.Item>
                  <Dropdown.Item eventKey="w">w</Dropdown.Item>
                  <Dropdown.Item eventKey="y">y</Dropdown.Item>
                  <Dropdown.Item eventKey="z">z</Dropdown.Item>
              </DropdownButton>
              <DropdownButton id="dropdown-basic-button" title="Sort"  size="sm" className = "filterButtons" onSelect={sortOnclick}>
                  <Dropdown.Item eventKey="city_namewithasc">City Name A-Z</Dropdown.Item>
                  <Dropdown.Item eventKey="city_namewithdes">City Name Z-A</Dropdown.Item>
                  <Dropdown.Item eventKey="County_namewithasc">County Name A-Z</Dropdown.Item>
                  <Dropdown.Item eventKey="County_namewithdes">County Name Z-A</Dropdown.Item>
                  <Dropdown.Item eventKey="average_tempwithasc">Average Temperature (low to high)</Dropdown.Item>
                  <Dropdown.Item eventKey="average_tempwithdes">Average Temperature (high to low)</Dropdown.Item>
                  <Dropdown.Item eventKey="populationwithasc">Population (low to high)</Dropdown.Item>
                  <Dropdown.Item eventKey="populationwithdes">Population (high to low)</Dropdown.Item>
                  <Dropdown.Item eventKey="sale_taxwithasc">Sale Tax (low to high)</Dropdown.Item>
                  <Dropdown.Item eventKey="sale_taxwithdes">Sale Tax (high to low)</Dropdown.Item>
                  <Dropdown.Item eventKey="sizewithasc">Size (low to high)</Dropdown.Item>
                  <Dropdown.Item eventKey="sizewithdes">Size (low to high)</Dropdown.Item>
              </DropdownButton>
              <Button variant="primary" title="Clean Filter" className = "filterButtons" size="sm" onClick = {clearOnclick}>Clear All</Button>
            </ div>
            <div style={{margin_right: '50px', marginLeft: '50px'}}>

            </ div>
            <Container className="row_class">
                <div>
                {isLoading ? (
                    <div class="d-flex justify-content-center">
                        <Spinner animation="border" role="status">
                            <span className="visually-hidden">Loading...</span>
                        </Spinner>
                    </div>
                ) :
                <BootstrapTable
                    remote
                    bootstrap4
                    condensed
                    bordered={ false }
                    hover
                    striped
                    noDataIndication="No Data"
                    keyField='id'
                    data={data}
                    columns={columnHeaders}
                    pagination={paginationFactory(paginationOption)}
                    rowEvents={tableRowEvents}
                />}
                </ div>
            </Container>
        </div>
    )
}

export default CityModule;
