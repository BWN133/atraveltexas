import requests
a = requests.get("https://api.atraveltx.me/api/city?page=1")
print(a.json())

#For City:
# Pagination : api/flight?page=1&pagesize=somenumber
# filtering:
# sale_tax: api/city?st=1/2/3/4/5( 1 for 6-6.5, 2 for 6.5-7, 3 for 7-7.5, 4 for 7.5-8, 5(or anything except for 1,2,3,4) for >8)
# city_name: api/city?cn=letter a-z
# County_name: api/city?con= letter a - z
# population: api/city?p=1/2/3( 1 for 0 -10000, 2 for 10000 - 100000, 3 (or anything except for 1,2) for >100000
# size=api/city?size=1/2/3(1 for 0-5, 2 for 5-10, 3(or anything else except for 1,2) for >10)
# They can filter multiple things like: api/flight?cn=a&con=b
# ------------------------------------------
# Sorting:
# api/city?s=(attribute name in database)with(des/asc) des for descending, asc(or anything else except for des) for ascending
# for example:
# api/city?s=city_namewithasc
# staging CI for frontend is broken
