import React, {useEffect, useState} from 'react'
import {Button, Card, Col, Form, InputGroup, Row, Spinner} from 'react-bootstrap';

import 'react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css';
import 'react-bootstrap-table2-filter/dist/react-bootstrap-table2-filter.min.css';

import {useHistory} from "react-router-dom";

import {scaleQuantize} from "d3-scale";
import axios from "axios";

import {ComposableMap, Geographies, Geography} from "react-simple-maps";

import "./CountyMap.css";
import './Covid.css';
import ReactPaginate from "react-paginate";
import Table from "./Table";

import {
    useQueryParams,
    StringParam,
    NumberParam,
    ArrayParam,
    withDefault,
  } from 'use-query-params';

const geoUrl = "https://gist.githubusercontent.com/ThomasThoren/efc950f1d26356afc842/raw/ee13a068efca41348d15379e93cce552b90574d0/texas-counties.json";

const sortOptions = [
    {label: 'County Name (asc)', value: 'county_namewithasc'},
    {label: 'County Name (des)', value: 'county_namewithdes'},
    {label: 'Cases (asc)', value: 'caseswithasc'},
    {label: 'Cases (des)', value: 'caseswithdes'},
    {label: 'Deaths (asc)', value: 'deathswithasc'},
    {label: 'Deaths (des)', value: 'deathswithdes'},
    {label: 'Infection Rate (asc)', value: 'infection_ratewithasc'},
    {label: 'Infection Rate (des)', value: 'infection_ratewithdes'},
    {label: 'Risk Level (asc)', value: 'overall_risk_level_IGNOREwithasc'},
    {label: 'Risk Level (des)', value: 'overall_risk_level_IGNOREwithdes'},
    {label: 'Transmission Level (asc)', value: 'cdc_transmission_level_IGNOREwithasc'},
    {label: 'Transmission Level (des)', value: 'cdc_transmission_level_IGNOREwithdes'},
    {label: 'ICU Bed Capacity Level (asc)', value: 'icu_bed_capacitywithasc'},
    {label: 'ICU Bed Capacity (des)', value: 'icu_bed_capacitywithdes'},
    {label: 'ICU Bed Usage (asc)', value: 'icu_bed_current_usagewithasc'},
    {label: 'ICU Bed Usage (des)', value: 'icu_bed_current_usagewithdes'}
];

const casesFilterOptions = [
    {label: '-', value: -1},
    {label: '0 - 1,000', value: 1},
    {label: '1,001 - 5,000', value: 2},
    {label: '5,001 - 10,000', value: 3},
    {label: '10,001+', value: 4}
];

const deathsFilterOptions = [
    {label: '-', value: -1},
    {label: '0 - 20', value: 1},
    {label: '20 - 60', value: 2},
    {label: '60 - 180', value: 3},
    {label: '180 - 540', value: 4},
    {label: '540+', value: 5}
];

const riskLevelFilterOptions = [
    {label: '-', value: -1},
    {label: 'None', value: 'n'},
    {label: 'Low', value: 'l'},
    {label: 'Medium', value: 'm'},
    {label: 'High', value: 'h'},
    {label: 'Severe', value: 's'}
];

const transmissionLevelFilterOptions = [
    {label: '-', value: -1},
    {label: 'Low', value: 'l'},
    {label: 'Moderate', value: 'm'},
    {label: 'High', value: 'h'},
    {label: 'Severe', value: 's'}
];

const infectionRateFilterOptions = [
    {label: '-', value: -1},
    {label: '0 - 0.5', value: 1},
    {label: '0.5 - 1', value: 2},
    {label: '1+', value: 3}
];

const icuBedFilterOptions = [
    {label: '-', value: -1},
    {label: '0 - 10', value: 1},
    {label: '10 - 50', value: 2},
    {label: '50 - 100', value: 3},
    {label: '100 - 500', value: 4},
    {label: '500+', value: 5}
];

const colorScale = scaleQuantize()
    .domain([1, 5])
    .range([
        "#cccccc",
        // "#ffedea",
        "#ffcec5",
        // "#ffad9f",
        "#ff8a75",
        // "#ff5533",
        "#e2492d",
        // "#be3d26",
        "#9a311f",
        // "#782618"
    ]);

const Covid = () => {
    /* React Pagination Variables */
    const [queryParams, setQueryParams] = useQueryParams({
        page: withDefault(NumberParam, 1),
        pagesize: withDefault(NumberParam, 20),
        c: NumberParam,
        d: NumberParam,
        rl: StringParam,
        tl: StringParam,
        ir: NumberParam,
        ibc: NumberParam,
        ibu: NumberParam,
        f: StringParam,
        s: withDefault(StringParam, "county_namewithasc"),
    });
    const [update, setUpdate] = useState(queryParams); // I CANNOT TELL YOU WHY BUT THIS NEEDS TO HAPPEN FOR HTE PAGE TO RELOAD
    // const [queryParams, setQueryParams] = useState({page: 1, pageSize: 20, sortBy: 'default'});
    const [totalEntries, setTotalEntries] = useState(0);
    const pageCount = Math.ceil(totalEntries / queryParams.pagesize);

    // Showing x to y of z
    const showingMessage = `Showing ${(queryParams.page - 1) * queryParams.pagesize + 1} to ${Math.min(queryParams.page * queryParams.pagesize, totalEntries)} of ${totalEntries}`;

    const handlePageChange = (data) => {
        setQueryParams({
            ...queryParams,
            page: data.selected + 1
        });
        setUpdate({
            ...queryParams,
            page: data.selected + 1
        });
    }

    const search = () => {
        setQueryParams({
            ...queryParams,
            f: update.f,
            page: 1
        });
        setUpdate({
            ...queryParams,
            f: update.f,
            page: 1
        });
    }

    const searchEnter = (e) => {
        e.preventDefault();
        if (e.key === "Enter" && e.keyCode === 13) {
            search();
        }
    }
    /**************/


    const [covidData, setDataC] = useState([]);
    const [isLoading, setLoading] = useState(true);
    const history = useHistory();

    /* Texas Geo Fields
    /*    field1: "48",
    /*    STATEFP: "48",
    /*    STUSPS: "TX",
    /*    NAME: "Texas",
    /*    xmin: "-106.645646",
    /*    ymin: "25.837377",
    /*    xmax: "-93.508292",
    /*    ymax: "36.500704"
    /*/

    const texasProjection = {
        scale: 1500,
        rotate: [98, 7, 0]
    }

    useEffect(() => {
        const getData = async () => {
            setLoading(true);
            const response = await axios.get('https://api.atraveltx.me/api/covid', {params: queryParams});
            setDataC(response.data.result);
            setTotalEntries(response.data.count);
            setLoading(false);
        };
        getData()
    }, [queryParams]);

    return (

        <React.Fragment>
            <div className="container">
                <div className="row" style={{padding: 10}}>
                    <div className="col-md-10 offset-md-1">
                        <Card border="white">
                            <Card.Title style={{textAlign: 'center'}}>
                                Covid Infection Rates by County
                            </Card.Title>
                            <Card.Body>
                                <ComposableMap
                                    width={800}
                                    height={300}
                                    projection="geoAlbers"
                                    projectionConfig={texasProjection}>
                                    <Geographies geography={geoUrl}>
                                        {({geographies}) =>
                                            // eslint-disable-next-line array-callback-return
                                            geographies.map(geo => {
                                                if (covidData) {
                                                    const cur = covidData.find(s => s.fips.toString() === "48" + geo.properties.COUNTYFP);
                                                    return (
                                                        <Geography key={geo.rsmKey} geography={geo}
                                                                   fill={colorScale(cur ? cur.overall_risk_level_IGNORE : 0)}
                                                                   style={{
                                                                       default: {outline: "none"},
                                                                       hover: {outline: "none"},
                                                                       pressed: {outline: "none"},
                                                                   }}/>)
                                                }
                                            })
                                        }
                                    </Geographies>
                                </ComposableMap>
                            </Card.Body>
                        </Card>

                    </div>
                </div>

                <Row style={{padding: 20, justifyContent: 'center'}}>
                    <Col sm={12} md={6}>
                        <Form.Group style={{display: 'flex'}}>
                            <Form.Control
                                value={update.f}
                                onChange={e => setUpdate({...queryParams, f: e.target.value})}
                                onKeyUp={searchEnter}
                            />
                            <Button onClick={search}>Search</Button>
                        </Form.Group>
                    </Col>
                </Row>

                <Row style={{padding: 5}}>
                    <Col sm={12} md={4} lg={4}>
                        <InputGroup style={{display: 'flex'}} size="sm">
                            <InputGroup.Text>Sort by:</InputGroup.Text>
                            <Form.Select defaultValue="county_namewithasc" value={queryParams.s}
                                         onChange={e => {setQueryParams({
                                             ...queryParams, 
                                             s: e.target.value,
                                             page: 1
                                         }); setUpdate({
                                             ...queryParams, 
                                             s: e.target.value,
                                             page: 1
                                        });}}>
                                {sortOptions.map((label) => (<option value={label.value}>{label.label}</option>))}
                            </Form.Select>
                        </InputGroup>

                    </Col>

                    <Col sm={12} md={4} lg={4}>
                        <InputGroup style={{display: 'flex'}} size="sm">
                            <InputGroup.Text>Filter Cases:</InputGroup.Text>
                            <Form.Select defaultValue="-" value={queryParams.c}
                                         onChange={e => {setQueryParams({
                                             ...queryParams,
                                             c: e.target.value === '-1' ? undefined : e.target.value,
                                             page: 1
                                         }); setUpdate({
                                            ...queryParams,
                                            c: e.target.value === '-1' ? undefined : e.target.value,
                                            page: 1
                                        });}}>
                                {casesFilterOptions.map((label) => (
                                    <option value={label.value}>{label.label}</option>))}
                            </Form.Select>
                        </InputGroup>

                    </Col>

                    <Col sm={12} md={4} lg={4}>
                        <InputGroup style={{display: 'flex'}} size="sm">
                            <InputGroup.Text>Filter Deaths:</InputGroup.Text>
                            <Form.Select defaultValue="-" value={queryParams.d}
                                         onChange={e => {setQueryParams({
                                             ...queryParams,
                                             d: e.target.value === '-1' ? undefined : e.target.value,
                                             page: 1
                                         }); setUpdate({
                                            ...queryParams,
                                            d: e.target.value === '-1' ? undefined : e.target.value,
                                            page: 1
                                        });}}>
                                {deathsFilterOptions.map((label) => (
                                    <option value={label.value}>{label.label}</option>))}
                            </Form.Select>
                        </InputGroup>

                    </Col>

                </Row>
                <Row style={{padding: 5}}>

                    <Col sm={12} md={4} lg={4}>
                        <InputGroup style={{display: 'flex'}} size="sm">
                            <InputGroup.Text>Filter Risk Level:</InputGroup.Text>
                            <Form.Select defaultValue="-" value={queryParams.rl}
                                         onChange={e => {setQueryParams({
                                             ...queryParams,
                                             rl: e.target.value === '-1' ? undefined : e.target.value,
                                             page: 1
                                         }); setUpdate({
                                            ...queryParams,
                                            rl: e.target.value === '-1' ? undefined : e.target.value,
                                            page: 1
                                        });}}>
                                {riskLevelFilterOptions.map((label) => (
                                    <option value={label.value}>{label.label}</option>))}
                            </Form.Select>
                        </InputGroup>

                    </Col>

                    <Col sm={12} md={4} lg={4}>
                        <InputGroup style={{display: 'flex'}} size="sm">
                            <InputGroup.Text>Filter Transmission Level:</InputGroup.Text>
                            <Form.Select defaultValue="-" value={queryParams.tl}
                                         onChange={e => {setQueryParams({
                                             ...queryParams,
                                             tl: e.target.value === '-1' ? undefined : e.target.value,
                                             page: 1
                                         }); setUpdate({
                                            ...queryParams,
                                            tl: e.target.value === '-1' ? undefined : e.target.value,
                                            page: 1
                                        })}}>
                                {transmissionLevelFilterOptions.map((label) => (
                                    <option value={label.value}>{label.label}</option>))}
                            </Form.Select>
                        </InputGroup>

                    </Col>

                    <Col sm={12} md={4} lg={4}>
                        <InputGroup style={{display: 'flex'}} size="sm">
                            <InputGroup.Text>Filter Infection Rate:</InputGroup.Text>
                            <Form.Select defaultValue="-" value={queryParams.ir}
                                         onChange={e => {setQueryParams({
                                             ...queryParams,
                                             ir: e.target.value === '-1' ? undefined : e.target.value,
                                             page: 1
                                         }); setUpdate({
                                            ...queryParams,
                                            ir: e.target.value === '-1' ? undefined : e.target.value,
                                            page: 1
                                        })}}>
                                {infectionRateFilterOptions.map((label) => (
                                    <option value={label.value}>{label.label}</option>))}
                            </Form.Select>
                        </InputGroup>

                    </Col>

                </Row>
                <Row style={{padding: 10, justifyContent: "center"}}>

                <Col sm={12} md={4} lg={4}>
                        <InputGroup style={{display: 'flex'}} size="sm">
                            <InputGroup.Text>Filter ICU Bed Capacity:</InputGroup.Text>
                            <Form.Select defaultValue="-" value={queryParams.ibc}
                                         onChange={e => {setQueryParams({
                                             ...queryParams,
                                             ibc: e.target.value === '-1' ? undefined : e.target.value,
                                             page: 1
                                         }); setUpdate({
                                            ...queryParams,
                                            ibc: e.target.value === '-1' ? undefined : e.target.value,
                                            page: 1
                                        });}}>
                                {icuBedFilterOptions.map((label) => (
                                    <option value={label.value}>{label.label}</option>))}
                            </Form.Select>
                        </InputGroup>

                    </Col>

                    <Col sm={12} md={4} lg={4}>
                        <InputGroup style={{display: 'flex'}} size="sm">
                            <InputGroup.Text className="small">Filter ICU Bed Usage:</InputGroup.Text>
                            <Form.Select defaultValue="-" value={queryParams.ibu}
                                         onChange={e => {setQueryParams({
                                             ...queryParams,
                                             ibu: e.target.value === '-1' ? undefined : e.target.value,
                                             page: 1
                                         }); setUpdate({
                                            ...queryParams,
                                            ibu: e.target.value === '-1' ? undefined : e.target.value,
                                            page: 1
                                        });}}>
                                {icuBedFilterOptions.map((label) => (
                                    <option value={label.value}>{label.label}</option>))}
                            </Form.Select>
                        </InputGroup>

                    </Col>
                </Row>

                <Row style={{padding: 10}}>
                    <Col md={12}>
                        {isLoading ? (
                            <div className="d-flex justify-content-center">
                                <Spinner animation="border" role="status">
                                    <span className="visually-hidden">Loading...</span>
                                </Spinner>
                            </div>
                        ) : (
                            <Table stats={covidData} history={history} isLoading={isLoading}
                                   searchTerms={queryParams.f}/>)
                        }
                    </Col>
                </Row>
                <Row className="justify-content-between" style={{padding: 10}}>
                    <Col md={3}>
                        {showingMessage}
                    </Col>
                    <Col md={"auto"}>
                        <ReactPaginate
                            previousLabel={'<'}
                            nextLabel={'>'}
                            breakLabel={'...'}
                            pageCount={pageCount}
                            forcePage={queryParams.page - 1}
                            marginPagesDisplayed={1}
                            pageRangeDisplayed={3}
                            onPageChange={handlePageChange}
                            subContainerClassName={'pages pagination'}
                            breakClassName={'page-item'}
                            breakLinkClassName={'page-link'}
                            containerClassName={'pagination'}
                            pageClassName={'page-item'}
                            pageLinkClassName={'page-link'}
                            previousClassName={'page-item'}
                            previousLinkClassName={'page-link'}
                            nextClassName={'page-item'}
                            nextLinkClassName={'page-link'}
                            activeClassName={'active'}
                        />
                    </Col>
                </Row>

            </div>
        </React.Fragment>

    );
}

export default Covid;