import React, {useEffect, useState} from "react";
import {Button, Card, Col, ListGroup, ListGroupItem, Row} from "react-bootstrap";

import BootstrapTable from "react-bootstrap-table-next";
import filterFactory, {textFilter} from "react-bootstrap-table2-filter";
import paginationFactory from 'react-bootstrap-table2-paginator';
import 'react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css';
import 'react-bootstrap-table2-filter/dist/react-bootstrap-table2-filter.min.css';

import {useHistory, useParams} from "react-router-dom";


import {
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
    Legend,
    LineChart,
    Line,
} from "recharts";

import axios from "axios";
import {Link} from "react-router-dom";

function County() {
    const countyName = useParams().countyName;

    const [covid_county_data, setData] = useState([]);
    const [cities_in_county, setCities] = useState([]);
    const [inbound_flights, setFlights] = useState([]);

    const [time_series_cases_data, setCasesTimeseries] = useState([]);
    const [time_series_data_deaths, setDeathTimeSeries] = useState([]);

    const [loaded, setLoaded] = useState(false);
    const history = useHistory();

    const [graph_values_cases, graphSetCaseValues] = useState([]);
    const [graph_values_deaths, graphSetDeathValues] = useState([]);

    useEffect(() => {
        const getData = async () => {
            axios.get(`https://api.atraveltx.me/api/covid/county_name=${countyName}`).then((response) => {
                setData(response.data);
                setCities(response.data.cities);
                setCasesTimeseries(response.data.values_time_series.cases);
                setDeathTimeSeries(response.data.values_time_series.deaths);

                response.data.cities.forEach(city => {
                    axios.get(`https://api.atraveltx.me//api/flight/city=${city}`).then(response => {
                        if (response.data.length > 0)
                            setFlights(response.data);
                    });
                });

            });
        }

        getData();

    },[]);

    useEffect(() => {
        graphSetCaseValues(graphValuesConvertCases(time_series_cases_data));
    }, [time_series_cases_data]);

    useEffect(() => {
        graphSetDeathValues(graphValuesConvertDeaths(time_series_data_deaths));
    }, [time_series_data_deaths]);

    function graphValuesConvertCases(obj) {
        let temp = [];
        for (const dataPoint in obj) {
            temp.push({'name': dataPoint, 'Cases': obj[dataPoint]});
        }
        return temp;
    }

    function graphValuesConvertDeaths(obj) {
        let temp = [];
        for (const dataPoint in obj) {
            temp.push({'name': dataPoint, 'Deaths': obj[dataPoint]});
        }
        return temp;
    }


    const columnHeaders = [{
        dataField: 'departure_airport',
        text: 'Departing',
        sort: true,
        filter: textFilter({style: {fontSize: 10}})
    }, {
        dataField: 'arrival_airport',
        text: 'Arriving',
        sort: true
    }, {
        dataField: 'flight_date',
        text: 'Date',
        sort: true
    }
    ];

    useEffect(() => {
        if (covid_county_data !== undefined && cities_in_county !== undefined && inbound_flights !== undefined && time_series_cases_data !== undefined) {
            setLoaded(true);
        }
    }, [covid_county_data, cities_in_county, inbound_flights, time_series_cases_data]);

    const incomingFlightRowLinks = {
        onClick: (e, row, rowIndex) => {
            history.push("/flight/" + row.id);
        }
    }

    if (loaded) {

        return (
            <React.Fragment>
                <div className="container py-3">
                    <header>
                        <div className="pricing-header p-3 pb-md-4 mx-auto text-center">
                            <h1 className="display-4 fw-normal">{covid_county_data.county_name}</h1>
                            <p className="fs-5 text-muted">Everyone in {covid_county_data.county_name}, should wear
                                a mask in
                                public, indoor settings. Mask requirements might vary from place to place. Make sure you
                                follow local laws, rules, regulations or guidance.
                            </p>
                        </div>
                    </header>


                    <Row>
                        <Col className="text-center">
                            <Button className="btn-sm"
                                    style={{background: '#1DA1F2', borderColor: '#1DA1F2', margin: '3px'}}
                                    href="https://twitter.com/cdcgov">Follow the CDC on Twitter</Button>
                            <Button className="btn-sm"
                                    style={{background: '#1DA1F2', borderColor: '#1DA1F2', margin: '3px'}}
                                    href="https://twitter.com/TexasDSHS">Follow the Texas Department of State Health
                                Services on Twitter</Button>
                        </Col>

                    </Row>

                    <main>
                        <Row className="py-3">
                            <Col className="col-md-4">
                                <Card>
                                    <Card.Header>
                                        <h6 className="display-8 text-center">County Stats</h6>
                                    </Card.Header>
                                    <Card.Body>
                                        <table className="table table-condensed table-borderless table-striped"
                                               style={{fontSize: 12}}>
                                            <thead>
                                            <tr>

                                                <th scope="col">Name</th>
                                                <th scope="col">Value</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <th scope="row">Population</th>
                                                <td>{covid_county_data.population}</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Cases</th>
                                                <td>{covid_county_data.cases || '-'}</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Case Density</th>
                                                <td>{covid_county_data.case_density || '-'}</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Cases (% of Population)</th>
                                                <td>{Math.round(covid_county_data.cases / covid_county_data.population * 1000) / 10}%</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Deaths</th>
                                                <td>{covid_county_data.deaths || 0}</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Deaths (% of Cases)</th>
                                                <td>{Math.round(covid_county_data.deaths / covid_county_data.cases * 1000) / 10}%</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">ICU Bed Capacity</th>
                                                <td>{covid_county_data.icu_bed_capacity || '-'}</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">ICU Bed Usage</th>
                                                <td>{covid_county_data.icu_bed_current_usage || '-'}</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Infection Rate</th>
                                                <td>{covid_county_data.infection_rate || '-'}</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Risk Level</th>
                                                <td>{covid_county_data.risk_level || '-'}</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">CDC Transmission Level</th>
                                                <td>{covid_county_data.cdc_transmission_level || '-'}</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </Card.Body>
                                </Card>
                            </Col>

                            <Col className="col-md-8">
                                <Card>
                                    <Card.Title style={{textAlign: 'center'}}>
                                        Covid Metrics - Rolling 7 Days
                                    </Card.Title>
                                    <Card.Body>
                                        <Row>
                                            <LineChart
                                                width={800}
                                                height={250}
                                                data={graph_values_cases}
                                                margin={{
                                                    top: 5,
                                                    right: 30,
                                                    left: 20,
                                                    bottom: 5,
                                                }}
                                            >
                                                <CartesianGrid strokeDasharray="3 3"/>
                                                <XAxis dataKey="name"/>
                                                <YAxis/>
                                                <Tooltip/>
                                                <Legend/>
                                                <Line type="monotone" dataKey="Cases" stroke="#F4B400"/>
                                            </LineChart>
                                        </Row>
                                        <Row>
                                            <LineChart
                                                width={800}
                                                height={250}
                                                data={graph_values_deaths}
                                                margin={{
                                                    top: 5,
                                                    right: 30,
                                                    left: 20,
                                                    bottom: 5,
                                                }}
                                            >
                                                <CartesianGrid strokeDasharray="3 3"/>
                                                <XAxis dataKey="name"/>
                                                <YAxis/>
                                                <Tooltip/>
                                                <Legend/>
                                                <Line type="monotone" dataKey="Deaths" stroke="#DB4437"/>
                                            </LineChart>
                                        </Row>

                                    </Card.Body>
                                </Card>
                            </Col>
                        </Row>

                        <Row>
                            <Col className="col-md-3">
                                <Card>
                                    <Card.Header>
                                        <h6 className="text-center">Cities in this county</h6>
                                    </Card.Header>
                                    <Card.Body>
                                        <ListGroup variant="flush">
                                            {

                                                cities_in_county.map((city) =>
                                                    <ListGroupItem>
                                                        <Link to={`/cityModule/${city}`}>{city}</Link>
                                                    </ListGroupItem>
                                                )}

                                        </ListGroup>
                                    </Card.Body>
                                </Card>
                            </Col>

                            <Col className="col-md-9">
                                <Card>
                                    <Card.Header>
                                        <h6 className="text-center">Available Incoming Flights</h6>
                                    </Card.Header>
                                    <Card.Body>
                                        <div style={{fontSize: 13}}>
                                            {inbound_flights.length === 0 ? ("No flights inbound ") : (<BootstrapTable
                                                bootstrap4 hover
                                                condensed keyField="id"
                                                data={inbound_flights}
                                                columns={columnHeaders}
                                                rowEvents={incomingFlightRowLinks}
                                                filter={filterFactory()}
                                                pagination={paginationFactory({
                                                    sizePerPage: 10,
                                                    showTotal: true
                                                })}
                                            />)}

                                        </div>
                                    </Card.Body>
                                </Card>
                            </Col>
                        </Row>

                    </main>

                    <footer className="pt-4 my-md-5 pt-md-5 border-top">
                        <Row>

                        </Row>
                    </footer>
                </div>
            </React.Fragment>

        );
    } else {
        return (
            <React.Fragment>
                <Row className="py-3">
                    <div className="text-center">
                        <div className="spinner-border" role="status">
                        </div>
                    </div>
                </Row>
                <div>Loading...</div>


            </React.Fragment>
        )
    }
}

export default County;