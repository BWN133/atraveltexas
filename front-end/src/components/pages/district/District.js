import React, {Component} from 'react'
import Table from "./Table-District";
import {Badge, Card, CardImg, Col, Container, Image, ListGroup, ListGroupItem, Pagination, Row, Tab} from "react-bootstrap";

import TransitHelpImage from '../../../static_resources/transit-help-image.png';
import WalkHelpImage from '../../../static_resources/walkability-help-image.png';
import BikeHelpImage from '../../../static_resources/bikeability-help-image.png';

class District extends Component {
    state = {};

    render() {
        return (
            <React.Fragment>
                <Container>
                    <h1>
                        Districts
                    </h1>
                    <p className="lead"
                       style={{"max-width": "60%", 'margin': 'auto', "text-align": 'center', padding: 15}}>
                        District information will display data such as the zip codes in a county, the walkability score
                        of
                        a zip code, or even the Covid-19 transmission rate so that you can make an informed decision
                        about where in Texas you'd like to visit.
                    </p>
                    <Row style={{padding: 15}}>
                        <Col md="4" style={{"text-align": "center"}}>
                            <Image src={WalkHelpImage} style={{width: "50%"}}/>

                            <Card>
                                <Card.Header style={{"text-align": "center"}}>
                                    3 Most Walkable Counties
                                </Card.Header>
                                <Card.Body>
                                    <ListGroup as="ol" numbered>
                                        <ListGroupItem>
                                            Hays County : 98
                                        </ListGroupItem>
                                        <ListGroupItem>
                                            Harris County : 82
                                        </ListGroupItem>
                                        <ListGroupItem>
                                            Travis County : 28
                                        </ListGroupItem>
                                    </ListGroup>

                                </Card.Body>
                            </Card>
                        </Col>
                        <Col md="4" style={{"text-align": "center"}}>
                            <Image src={BikeHelpImage} style={{width: "50%"}}/>

                            <Card>
                                <Card.Header style={{"text-align": "center"}}>
                                    3 Most Bikeable Counties
                                </Card.Header>
                                <Card.Body>
                                    <ListGroup as="ol" numbered>
                                        <ListGroupItem>
                                            Hays County : 87
                                        </ListGroupItem>
                                        <ListGroupItem>
                                            Harris County : 53
                                        </ListGroupItem>
                                        <ListGroupItem>
                                            Travis County : 32
                                        </ListGroupItem>
                                    </ListGroup>

                                </Card.Body>
                            </Card>
                        </Col>
                        <Col md="4" style={{"text-align": "center"}}>
                            <Image src={TransitHelpImage} style={{width: "50%"}}/>

                            <Card>
                                <Card.Header style={{"text-align": "center"}}>
                                    3 Most Transitable Counties
                                </Card.Header>
                                <Card.Body>
                                    <ListGroup as="ol" numbered>
                                        <ListGroupItem>
                                            Hays County : 99
                                        </ListGroupItem>
                                    </ListGroup>

                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                    <Row style={{padding: 10}}>
                        <Col>
                            <Card>
                                <Table/>
                            </Card>
                        </Col>
                    </Row>
                    <div style={{ display: "flex"}}>
                        <p>Total: 3 items</p>
                        <Pagination style={{ marginLeft: "auto"}}>
                            <Pagination.Item active activeLabel="">{1}</Pagination.Item>
                        </Pagination>
                    </div>
                </Container>
            </React.Fragment>
        );
    }

}

export default District;