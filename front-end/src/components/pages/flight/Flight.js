import { Container, Row, Col, Button, Modal, Form, FloatingLabel, Spinner, InputGroup } from 'react-bootstrap';
import React, { useEffect, useState } from 'react'
import axios from "axios"
import './Flight.css';
import FlightCard from './FlightCard';
import ReactPaginate from 'react-paginate';
import Select from 'react-select';
import FlightCardHighlight from './FlightCardHighlight';

import {
    useQueryParams,
    StringParam,
    NumberParam,
    withDefault,
  } from 'use-query-params';

const pageSizes = [
    { label: 10, value: 10 },
    { label: 20, value: 20 },
    { label: 40, value: 40 },
];

const sortOptions = [
    { label: "Departure Code(asc)", value: "departure_icaowithasc"},
    { label: "Departure Code(des)", value: "departure_icaowithdes"},
    { label: "Departure Time(asc)", value: "departure_scheduledwithasc"},
    { label: "Departure Time(des)", value: "departure_scheduledwithdes"},
    { label: "Arrival Code(asc)", value: "arrival_icaowithasc"},
    { label: "Arrival Code(des)", value: "arrival_icaowithdes"},
    { label: "Arrival Time(asc)", value: "arrival_scheduledwithasc"},
    { label: "Arrival Time(des)", value: "arrival_scheduledwithdes"},
    { label: "Airline(asc)", value: "airline_namewithasc"},
    { label: "Airline(des)", value: "airline_namewithdes"},
];

function Flight () {
    const [flightData, setFlightData] = useState([]);
    const [queryParam, setQueryParam] = useQueryParams({
        page: withDefault(NumberParam, 1),
        pagesize: withDefault(NumberParam, 20),
        di: StringParam,
        dsmin: StringParam,
        dsmax: StringParam,
        asmin: StringParam,
        asmax: StringParam,
        ai: StringParam,
        an: StringParam,
        f: StringParam,
        s: withDefault(StringParam, "departure_icaowithasc"),
    });

    const [totalEntries, setTotalEntries] = useState(0);
    const [isLoading, setIsLoading] = useState(true);

    const pageCount = Math.ceil(totalEntries/queryParam.pagesize);

    const [formState, setFormState] = useState(queryParam);

    // Get data
    useEffect(() => {
        const getData = async () => {
            setIsLoading(true);
            const response = await axios.get("https://api.atraveltx.me/api/flight", { params: queryParam});
            setFlightData(response.data.result);
            setTotalEntries(response.data.count);
            setIsLoading(false);
        }
        getData();
    }, [queryParam]); // THIS LITERALLY MAKES NO SENSE, WHY DOES UPDATING QUERYPARAM DO NOTHING

    // Set up labels for select options
    const [labels, setLabels] = useState([]);

    useEffect(() => {
        const getLabels = async() => {
            const response = await axios.get("https://api.atraveltx.me/api/flight/info");
            response.data.departure_icao.sort();
            response.data.arrival_icao.sort();
            response.data.airline_name.sort();
            setLabels(response.data);
        }
        getLabels();
    }, []);

    // Change page size
    const handlePageSize = (selectedOption) => {
        setQueryParam({
            ...queryParam,
            pagesize: selectedOption.value,
            page: 1
        });
        setFormState({
            ...queryParam,
            pagesize: selectedOption.value,
            page: 1
        });
    };

    // Change page number
    const handlePageClick = (data) => {
        setQueryParam({
            ...queryParam,
            page: data.selected + 1
        });
        setFormState({
            ...queryParam,
            page: data.selected + 1
        });
    }

    // Handles if modal is shown
    const [showModal, setShowModal] = useState(false);

    const handleCloseModal = () => setShowModal(false);
    const handleShowModal = () => setShowModal(true);

    // Handles submit button for modal
    const onFormSubmit = () => {
        let temp = {
            ...queryParam, 
            page: 1,
            di: formState.di, 
            ai: formState.ai, 
            an: formState.an, 
            dsmin: formState.dsmin, 
            dsmax: formState.dsmax,
            asmin: formState.asmin,
            asmax: formState.asmax
        };
        if (temp.ai === "Select one") {
            temp.ai = undefined;
        }
        if (temp.di === "Select one") {
            temp.di = undefined;
        }
        if (temp.an === "Select one") {
            temp.an = undefined;
        }
        if (temp.dsmin === "") {
            temp.dsmin = undefined;
        }
        if (temp.dsmax === "") {
            temp.dsmax = undefined;
        }
        if (temp.asmin === "") {
            temp.asmin = undefined;
        }
        if (temp.asmax === "") {
            temp.asmax = undefined;
        }
        
        setQueryParam(temp);
        handleCloseModal();
    }

    const onFormClear = () => {
        const rest = { 
            page: queryParam.page, 
            pagesize: queryParam.pagesize, 
            f: queryParam.f, 
            s: queryParam.s,
            di: undefined,
            dsmin: undefined,
            dsmax: undefined,
            asmin: undefined,
            asmax: undefined,
            ai: undefined,
            an: undefined
        };
        setFormState(rest);
        setQueryParam(rest);
        handleCloseModal();
    }

    const search = () => {
        let temp = {
            ...queryParam,
            f: formState.f
        };
        if (temp.f == "")
            temp.f = undefined;
        setFormState(temp);
        setQueryParam(temp);
    }

    const searchEnter = (e) => {
        e.preventDefault();
        if (e.key=== "Enter" && e.keyCode === 13) {
            search();
        }
    }

    const selectSort = (e) => {
        setQueryParam({...queryParam, s: e.target.value});
        setFormState({...queryParam, s: e.target.value});
    }

    return (
        <div>
            <Container>
                <h1>Flights to Texas</h1>
                <br/>
                
                <Form onSubmit={(e) => {e.preventDefault()}}>
                    <Row>
                        <Col md="1">
                            <Button onClick={handleShowModal}>
                                Filters
                            </Button>
                        </Col>
                        <Col md="4">
                            <InputGroup style={{display:'flex'}}>
                                <InputGroup.Text>Sort by:</InputGroup.Text>
                                <Form.Select defaultValue="departure_icaowithasc" value={queryParam.s} onChange={selectSort}>
                                    {sortOptions.map((label) => (<option value={label.value}>{label.label}</option>))}
                                </Form.Select>
                            </InputGroup>
                        </Col>
                        <Col md="1"/>
                        <Col md="6">
                            <Form.Group style={{display:'flex'}}>
                            <Form.Control
                                value={formState.f}
                                onChange={e => setFormState({...formState, f: e.target.value})}
                                onKeyUp={searchEnter}
                            />
                            <Button onClick={search}>Search</Button>
                            </Form.Group>
                        </Col>
                    </Row>
                </Form>

                {/* Filter modal */}
                <Modal show={showModal} onHide={handleCloseModal} size="lg">
                    <Modal.Header closeButton>
                        <Modal.Title>Filters</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form>

                        <Row className="mb-3">
                        <Form.Group>
                            <Form.Label>Departure ICAO</Form.Label>
                            <Form.Select defaultValue="Select one" value={formState.di} onChange={e => setFormState({...formState, di: e.target.value})}>
                                <option>Select one</option>
                                {labels?.departure_icao? labels.departure_icao.map((label) => (<option>{label}</option>)) : <></>}
                            </Form.Select>
                        </Form.Group>
                        </Row>
                        
                        <Form.Label>Departure Time (Give min and max)</Form.Label>
                        <Row className="mb-3">
                            <Form.Group as={Col} controlId="formGridEmail">
                                <FloatingLabel label="Min" >
                                    <Form.Control 
                                        placeholder="temp" 
                                        value={formState.dsmin} 
                                        onChange={e => setFormState({...formState, dsmin: e.target.value})}
                                    />
                                </FloatingLabel>
                                <Form.Text id="timeHelpBlock" muted>
                                    In format YYYY-MM-DDTHH:MM:SS
                                    Example: 2021-10-14T18:25:00
                                </Form.Text>
                                
                            </Form.Group>
                            <Form.Group as={Col} controlId="formGridEmail">
                                <FloatingLabel label="Max" >
                                <Form.Control size="sm" placeholder="temp" value={formState.dsmax} onChange={e => setFormState({...formState, dsmax: e.target.value})}/>
                                </FloatingLabel>
                                <Form.Text id="timeHelpBlock" muted>
                                    In format YYYY-MM-DDTHH:MM:SS
                                    Example: 2021-10-14T18:25:00
                                </Form.Text>
                            </Form.Group>
                        </Row>

                        <Row className="mb-3">
                        <Form.Group>
                            <Form.Label>Arrival ICAO</Form.Label>
                            <Form.Select defaultValue="Select one" value={formState.ai} onChange={e => setFormState({...formState, ai: e.target.value})}>
                                <option>Select one</option>
                                {labels?.arrival_icao? labels.arrival_icao.map((label) => (<option>{label}</option>)) : <></>}
                            </Form.Select>
                        </Form.Group>
                        </Row>
                        
                        <Form.Label>Arrival Time (Give min and max)</Form.Label>
                        <Row className="mb-3">
                            <Form.Group as={Col} controlId="formGridEmail">
                                <FloatingLabel label="Min" >
                                <Form.Control size="sm" placeholder="temp" value={formState.asmin} onChange={e => setFormState({...formState, asmin: e.target.value})}/>
                                </FloatingLabel>
                                <Form.Text id="timeHelpBlock" muted>
                                    In format YYYY-MM-DDTHH:MM:SS
                                    Example: 2021-10-14T18:25:00
                                </Form.Text>
                            </Form.Group>
                            <Form.Group as={Col} controlId="formGridEmail">
                                <FloatingLabel label="Max" >
                                <Form.Control size="sm" placeholder="temp" value={formState.asmax} onChange={e => setFormState({...formState, asmax: e.target.value})}/>
                                </FloatingLabel>
                                <Form.Text id="timeHelpBlock" muted>
                                    In format YYYY-MM-DDTHH:MM:SS
                                    Example: 2021-10-14T18:25:00
                                </Form.Text>
                            </Form.Group>
                        </Row>

                        <Row className="mb-3">
                        <Form.Group>
                            <Form.Label>Airline</Form.Label>
                            <Form.Select defaultValue="Select one" value={formState.an} onChange={e => setFormState({...formState, an: e.target.value})}>
                                <option>Select one</option>
                                {labels?.airline_name? labels.airline_name.map((label) => (<option>{label}</option>)) : <></>}
                            </Form.Select>
                        </Form.Group>
                        </Row>

                        </Form>
                    </Modal.Body>

                    <Modal.Footer>
                        {/* Need to change this button to handle changes */}
                        <Button onClick={onFormSubmit}>Submit</Button>
                        <Button onClick={onFormClear}>Clear Filter</Button>
                    </Modal.Footer>
                </Modal>
                <br/>

                {/* Map data to cards */}
                {isLoading ? (
                    <div class="d-flex justify-content-center">
                        <Spinner animation="border" role="status">
                            <span className="visually-hidden">Loading...</span>
                        </Spinner>
                    </div>
                ) : (
                queryParam?.f ? 
                    <FlightCardHighlight
                        flightData={flightData}
                        searchTerm={queryParam.f}
                    /> : 
                    <FlightCard
                        flightData={flightData}
                    />
                )}

                <br/>
                <Row>
                    <Col md={4} >
                        <p>
                            <b>Total entries: </b>{totalEntries}
                        </p>
                    </Col>
                    <Col md={2}style={{textAlign:'right'}}>
                        <p>
                            <b>Items per page: </b>
                        </p>
                    </Col>
                    <Col md={2}>
                        <div style={{width:'50%'}}>
                        <Select
                            autoFocus
                            options={pageSizes}
                            placeholder={queryParam.pagesize}
                            onChange={handlePageSize}
                            menuPlacement='auto'
                        />
                        </div>
                    </Col>
                    <Col md={4}>
                    <div style={{width:'100%'}} align='right'>
                        <ReactPaginate
                            previousLabel={'previous'}
                            nextLabel={'next'}
                            breakLabel={'...'}
                            pageCount={pageCount}
                            forcePage={queryParam.page - 1}
                            marginPagesDisplayed={1}
                            pageRangeDisplayed={3}
                            onPageChange={handlePageClick}
                            subContainerClassName={'pages pagination'}
                            breakClassName={'page-item'}
                            breakLinkClassName={'page-link'}
                            containerClassName={'pagination'}
                            pageClassName={'page-item'}
                            pageLinkClassName={'page-link'}
                            previousClassName={'page-item'}
                            previousLinkClassName={'page-link'}
                            nextClassName={'page-item'}
                            nextLinkClassName={'page-link'}
                            activeClassName={'active'}
                        />
                    </div>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}

export default Flight;