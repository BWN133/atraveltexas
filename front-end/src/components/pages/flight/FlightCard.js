import React from 'react'
import { Card, Col, ListGroup, ListGroupItem, Row } from 'react-bootstrap';

function FlightCard ({ flightData }) {
    console.log("flight Data with no highlight here!!!!!");
    console.log(flightData);
    if (flightData?.length !== 0)
    return (
        <Row id="hoverable">
            {flightData.map(data => (
                <Col sm={3} style={{marginBottom: '10px'}}>
                    <Card style={{ height: '100%', width: '100%' }}>
                        <Card.Img variant="top" src={"http://pics.avs.io/200/200/"+data.airline_iata+".png"} style={{objectFit: 'cover', height: '30vh'}}/>
                        <Card.Body>
                            <Card.Title>{data?.airline_name} {data?.flight_number}</Card.Title>
                        </Card.Body>
                        <ListGroup className="list-group-flush">
                            <ListGroupItem><b>Departure Code: </b>
                                {data?.departure_icao ?
                                ' '+data.departure_icao : ' Unknown'}
                            </ListGroupItem>
                            <ListGroupItem><b>Departure Time: </b>
                                {data?.departure_scheduled ?
                                ' '+data.departure_scheduled : ' Unknown'}
                            </ListGroupItem>
                            <ListGroupItem><b>Arrival Code: </b>
                                {data?.arrival_icao ?
                                ' '+data.arrival_icao : ' Unknown'}
                            </ListGroupItem>
                            <ListGroupItem><b>Arrival Time: </b>
                                {data?.arrival_scheduled ?
                                ' '+data.arrival_scheduled : ' Unknown'}
                            </ListGroupItem>
                            <ListGroupItem><b>Airline: </b>
                                {data?.airline_name?
                                ' '+data.airline_name : ' Unknown'}
                            </ListGroupItem>
                        </ListGroup>
                        <a href={"/flight/"+data.id} class="stretched-link"> </a>
                    </Card>
                </Col>
            ))}
        </Row>
    );
    return (<div>No results found.</div>);
}

export default FlightCard;
