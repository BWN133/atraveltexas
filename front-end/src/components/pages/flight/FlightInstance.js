import { Button, Card, CardDeck, Col, Container, Row } from "react-bootstrap";
import { useParams } from 'react-router';
import React, { useEffect, useState } from 'react'
import axios from "axios"
import { GoogleMap, withScriptjs, withGoogleMap, Marker } from 'react-google-maps';
import { useHistory } from "react-router-dom";

function FlightInstance () {
    const flightId = parseInt(useParams().flightId, 10);

    const [ data, setData ] = useState([]);

    useEffect(() => {
        const getData = async () => {
            const response = await axios.get("https://api.atraveltx.me/api/flight/id="+flightId);
            setData(response.data);
        }
        getData();
    }, []);

    const MyMapComponent = withScriptjs(withGoogleMap((props) =>
        <GoogleMap
            defaultZoom={14}
            defaultCenter={{ lat: props.lat, lng: props.lng }}
        >
            {/* {props.isMarkerShown && <Marker position={{ lat: -34.397, lng: 150.644 }} />} */}
        </GoogleMap>
    ))

    const history = useHistory();
    // history.goBack();
    console.log(data)

    return (
        <div>
            <Container>
                <h1>{data?.airline_name} {data?.flight_number}</h1>
                <MyMapComponent
                    lng={data.longitude}
                    lat={data.latitude}
                    googleMapURL={`https://maps.googleapis.com/maps/api/js?key=${process.env.REACT_APP_GOOGLE_KEY}&v=3.exp&libraries=geometry,drawing,places`}
                    loadingElement={<div style={{ height: `100%` }} />}
                    containerElement={<div style={{ height: `400px` }} />}
                    mapElement={<div style={{ height: `100%` }} />}
                />
                <br/>
                <Row>
                    <Col>
                        <Card>
                            <Card.Header>
                                <h4 className="display-8 text-center mb-4">Departure</h4>
                            </Card.Header>
                            <Card.Body>
                                <table className="table table-condensed table-borderless table-striped">
                                    <tbody>
                                    <tr>
                                        <th scope="row">Airport</th>
                                        <td>
                                            {data?.departure_airport ? 
                                            data.departure_airport : 'Unknown'}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Airport ICAO</th>
                                        <td>
                                            {data?.departure_icao ? 
                                            data.departure_icao : 'Unknown'}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Scheduled</th>
                                        <td>
                                            {data?.departure_scheduled ? 
                                            data.departure_scheduled : 'Unknown'}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Terminal</th>
                                        <td>
                                            {data?.departure_terminal ? 
                                            data.departure_terminal : 'Unknown'}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Gate</th>
                                        <td>
                                            {data?.departure_gate ? 
                                            data.departure_gate : 'Unknown'}
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col>
                        <Card>
                            <Card.Header>
                                <h4 className="display-8 text-center mb-4">Arrival</h4>
                            </Card.Header>
                            <Card.Body>
                                <table className="table table-condensed table-borderless table-striped">
                                    <tbody>
                                    <tr>
                                        <th scope="row">Airport</th>
                                        <td>
                                            {data?.arrival_airport ? 
                                            data.arrival_airport : 'Unknown'}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Airport ICAO</th>
                                        <td>
                                            {data?.arrival_icao ? 
                                            data.arrival_icao : 'Unknown'}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Scheduled</th>
                                        <td>
                                            {data?.arrival_scheduled ? 
                                            data.arrival_scheduled : 'Unknown'}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Terminal</th>
                                        <td>
                                            {data?.arrival_terminal ? 
                                            data.arrival_terminal : 'Unknown'}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Gate</th>
                                        <td>
                                            {data?.arrival_gate ? 
                                            data.arrival_gate : 'Unknown'}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Baggage Claim</th>
                                        <td>
                                            {data?.arrival_baggage ? 
                                            data.arrival_baggage : 'Unknown'}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">City</th>
                                        <td style={{textAlign: 'right'}}>
                                            {data?.city ? 
                                            data.city + " " : 'Unknown'}
                                            {data?.city ?
                                            <Button onClick={() => {
                                                history.push("/cityModule/" + data.city);
                                            }}>Learn more</Button> : "" }
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">County</th>
                                        <td style={{textAlign: 'right'}}>
                                            {data?.county ? 
                                            data.county + " " : 'Unknown'}
                                            {data?.county ?
                                            <Button onClick={() => {
                                                history.push("/covid/" + data.county);
                                            }}>Get Covid Rates</Button> : "" }
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
                <br/>
                <Row>
                    <Col>
                        <Card>
                            <Card.Header>
                                <h4 className="display-8 text-center mb-4">Airline</h4>
                            </Card.Header>
                            <Card.Img src={"http://pics.avs.io/200/200/"+data.airline_iata+".png"}
                            style={{width:"200px", height:"200px", alignSelf: 'center'}}/>
                            <Card.Body>
                                <table className="table table-condensed table-borderless table-striped">
                                    <tbody>
                                    <tr>
                                        <th scope="row">Name</th>
                                        <td>
                                            {data?.airline_name ? 
                                            data.airline_name : 'Unknown'}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Flight Number</th>
                                        <td>
                                            {data?.flight_number ? 
                                            data.flight_number : 'Unknown'}
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col>
                        {data?.codeshared_name ?
                            <Card>
                                <Card.Header>
                                    <h4 className="display-8 text-center mb-4">Codeshared Airline</h4>
                                </Card.Header>
                                <Card.Img src={"http://pics.avs.io/200/200/"+data.codeshared_iata.toUpperCase()+".png"}
                                style={{width:"200px", height:"200px", alignSelf: 'center'}}/>
                                <Card.Body>
                                    <table className="table table-condensed table-borderless table-striped">
                                        <tbody>
                                        <tr>
                                            <th scope="row">Name</th>
                                            <td>
                                                {data?.codeshared_name ? 
                                                data.codeshared_name : 'Unknown'}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Flight Number</th>
                                            <td>
                                                {data?.flight_number ? 
                                                data.flight_number : 'Unknown'}
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </Card.Body>
                            </Card> : <div></div>
                        }
                    </Col>
                </Row>
            </Container>
        </div>
    );
}

export default FlightInstance;