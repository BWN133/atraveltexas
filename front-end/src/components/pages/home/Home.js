import React from 'react'
import './Home.css'
import logo from "../../../static_resources/att_logo.png"
import home_page_background from "../../../static_resources/home_page_background.jpg"
import { Row, Col, Card } from 'react-bootstrap';
import flight_image from '../../../static_resources/flight_image.jpg'
import covid_image from '../../../static_resources/covid_image.jpg'
import district_image from '../../../static_resources/district_image.jpg'

function Home () {
    return (
        <div>
            <img src={home_page_background} style={{objectFit: 'cover', width: '100%',height: '100%', position:'absolute'}}/>
            <div className="titlepos">
                <div className="positionedLogo">
                    <img className="logo" src = {logo} />
                </div>
                <p id="Intro" className="text-center fs-2 fw-bold">Giving you a comfortable and safe trip in Texas</p>
            </div>
            <div className="posModules" id="hoverable">
                <Row style={{justifyContent: 'center'}}>
                    <Col sm={3}>
                        <Card>
                            <Card.Img variant="top" src={flight_image} style={{objectFit: 'cover', height: '25vh'}}/>
                            <Card.Body>
                                <Card.Title>Flights to Texas</Card.Title>
                            </Card.Body>
                            <a href="/flight" class="stretched-link"> </a>
                        </Card>
                    </Col>
                    <Col sm={3}>
                        <Card>
                            <Card.Img variant="top" src={district_image} style={{objectFit: 'cover', height: '25vh'}}/>
                            <Card.Body>
                                <Card.Title>City Information</Card.Title>
                            </Card.Body>
                            <a href="/cityMain" class="stretched-link"> </a>
                        </Card>
                    </Col>
                    <Col sm={3}>
                        <Card>
                            <Card.Img variant="top" src={covid_image} style={{objectFit: 'cover', height: '25vh'}}/>
                            <Card.Body>
                                <Card.Title>Covid-19 Rates</Card.Title>
                            </Card.Body>
                            <a href="/covid" class="stretched-link"> </a>
                        </Card>
                    </Col>
                </Row>
            </div>
        </div>
    );
}

export default Home;
