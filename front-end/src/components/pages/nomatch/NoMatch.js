import React from 'react'

function NoMatch () {
    return (
        <div>
            <p>404 Error</p>
        </div>
    );
}

export default NoMatch;