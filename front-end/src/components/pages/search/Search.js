import axios from "axios";
import { useEffect, useState } from "react";
import { Container, Form, Row, Col, Button, Spinner } from "react-bootstrap";
import SearchCityCard from "./SearchCityCard";
import SearchCovidCard from "./SearchCovidCard";
import SearchFlightCard from "./SearchFlightCard";

function Search () {
    const [queryParam, setQueryParam] = useState({
        page: 1,
        pagesize: 10
    });
    const [searchState, setSearchState] = useState({});

    const [flightData, setFlightData] = useState([]);
    const [cityData, setCityData] = useState([]);
    const [covidData, setCovidData] = useState([]);

    const [isLoadingFlight, setIsLoadingFlight] = useState(true);
    const [isLoadingCity, setIsLoadingCity] = useState(true);
    const [isLoadingCovid, setIsLoadingCovid] = useState(true);

    useEffect(() =>{
        const getFlightData = async () => {
            setIsLoadingFlight(true);
            const response = await axios.get("https://api.atraveltx.me/api/flight", { params: queryParam});
            setFlightData(response.data);
            setIsLoadingFlight(false);
        }

        const getCityData = async () => {
            setIsLoadingCity(true);
            const response = await axios.get("https://api.atraveltx.me/api/city", { params: queryParam});
            setCityData(response.data);
            setIsLoadingCity(false);
        }

        const getCovidData = async () => {
            setIsLoadingCovid(true);
            const response = await axios.get("https://api.atraveltx.me/api/covid", { params: queryParam});
            setCovidData(response.data);
            setIsLoadingCovid(false);
        }

        getFlightData();
        getCityData();
        getCovidData();
    }, [queryParam]);

    const search = () => {
        let temp = {
            ...queryParam,
            f: searchState.f
        };
        setQueryParam(temp);
    }

    const searchEnter = (e) => {
        e.preventDefault();
        if (e.key=== "Enter" && e.keyCode === 13) {
            search();
        }
    }

    return (
        <div>
            <Container>
                <h1>Search</h1>
                <br/>
                <Form onSubmit={(e) => {e.preventDefault()}}>
                    <Row>
                        <Col >
                            <Form.Group style={{display:'flex'}}>
                                <Form.Control
                                    value={searchState.f}
                                    onChange={e => setSearchState({...setSearchState, f: e.target.value})}
                                    onKeyUp={searchEnter}
                                />
                                <Button onClick={search}>Search</Button>
                            </Form.Group>
                        </Col>
                    </Row>
                </Form>
                <br/>
                <br/>

                <h3 className="borderedHeader">Flights</h3>
                <br/>
                {isLoadingFlight ? (
                    <div class="d-flex justify-content-center">
                        <Spinner animation="border" role="status">
                            <span className="visually-hidden">Loading...</span>
                        </Spinner>
                    </div>
                ) : (
                    flightData?.result?.length === 0 ? (
                        <div>No results found.</div>
                    ) : (
                        <>
                        <SearchFlightCard
                            flightData={flightData.result}
                            flightCount={flightData.count}
                            searchTerm={queryParam.f}
                        />
                        </>
                    )
                )}
                <br/>
                <br/>

                <h3 className="borderedHeader">City</h3>
                <br/>
                {isLoadingCity ? (
                    <div class="d-flex justify-content-center">
                        <Spinner animation="border" role="status">
                            <span className="visually-hidden">Loading...</span>
                        </Spinner>
                    </div>
                ) : (
                    cityData?.result?.length === 0 ? (
                        <div>No results found.</div>
                    ) : (
                        <SearchCityCard
                            cityData={cityData.result}
                            cityCount={cityData.count}
                            searchTerm={queryParam.f}
                        />
                    )
                )}
                <br/>
                <br/>

                <h3 className="borderedHeader">Covid</h3>
                <br/>
                {isLoadingCovid ? (
                    <div class="d-flex justify-content-center">
                        <Spinner animation="border" role="status">
                            <span className="visually-hidden">Loading...</span>
                        </Spinner>
                    </div>
                ) : (
                    covidData?.result?.length === 0 ? (
                        <div>No results found.</div>
                    ) : (
                        <SearchCovidCard
                            covidData={covidData.result}
                            covidCount={covidData.count}
                            searchTerm={queryParam.f}
                        />
                    )
                )}
                <br/>
            </Container>
        </div>
    );
}

export default Search;