import React from 'react'
import { Card, Col, ListGroup, ListGroupItem, Row } from 'react-bootstrap';
import Highlighter from 'react-highlight-words';

function SearchCovidCard({covidData, covidCount, searchTerm}) {
    console.log(covidData);
    const searchWords = searchTerm ? searchTerm.split(" ") : [""];

    // Need to change to get localStorage logic
    const countRemaining = covidCount > 10 ? covidCount - 10 : 0;

    return (
        <Row id="hoverable">
            {covidData.map(data => (
                <Col sm={3} style={{marginBottom: '10px'}}>
                    <Card className="searchCard">
                        <Card.Body>
                            <Card.Title>
                                <Highlighter
                                    highlightClassName="searchHighlight"
                                    searchWords={searchWords}
                                    textToHighlight={data?.county_name}
                                />
                            </Card.Title>
                        </Card.Body>
                        <ListGroup className="list-group-flush">
                            <ListGroupItem><b>Cases: </b>
                                <Highlighter
                                    highlightClassName="searchHighlight"
                                    searchWords={searchWords}
                                    textToHighlight={data?.cases ? 
                                        ' '+data.cases : ' Unknown'}
                                />
                            </ListGroupItem>
                            <ListGroupItem><b>Deaths: </b>
                                <Highlighter
                                    highlightClassName="searchHighlight"
                                    searchWords={searchWords}
                                    textToHighlight={data?.deaths ? 
                                        ' '+data.deaths : ' Unknown'}
                                />
                            </ListGroupItem>
                            <ListGroupItem><b>Transmission Level: </b>
                                <Highlighter
                                    highlightClassName="searchHighlight"
                                    searchWords={searchWords}
                                    textToHighlight={data?.cdc_transmission_level ? 
                                        ' '+data.cdc_transmission_level : ' Unknown'}
                                />
                            </ListGroupItem>
                            <ListGroupItem><b>Risk Level: </b>
                                <Highlighter
                                    highlightClassName="searchHighlight"
                                    searchWords={searchWords}
                                    textToHighlight={data?.risk_level ? 
                                        ' '+data.risk_level : ' Unknown'}
                                />
                            </ListGroupItem>
                        </ListGroup>
                        <a href={"/covid/"+data.county_name.substring(0, data.county_name.length - 7)} class="stretched-link"> </a>
                    </Card>
                </Col>
            ))}
            {countRemaining !== 0 ? 
                <Col sm={3} style={{marginBottom: '10px'}}>
                    <Card className="searchCard" style={{ height: '100%', width: '100%' }}>
                        <Card.Body>
                            <Card.Title>
                                See {countRemaining} more Covid search results.
                            </Card.Title>
                        </Card.Body>
                        <a href={searchTerm ? 
                        `/covid/?page=1&pagesize=20&s=county_namewithasc&f=${searchTerm}`
                        :`/covid/?page=1&pagesize=20&s=county_namewithasc`} class="stretched-link"/>
                    </Card>
                </Col> 
                : <></>}
        </Row>
    )
}

export default SearchCovidCard;