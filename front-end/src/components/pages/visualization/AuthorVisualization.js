import React, {useEffect, useState} from "react";
import axios from "axios";
import {BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer} from 'recharts';
import {Card, Row, Spinner} from "react-bootstrap";


function AuthorVisualization() {
    const [authorsByNationality, setAuthors] = useState([]);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        const getData = async () => {
            axios.get('https://api.prideinwriting.me/api/authors').then((response) => {
                const temp = new Map();
                response.data.data.forEach(author => {
                    let n = temp.get(author.nationality) || 0;
                    temp.set(author.nationality, n + 1);
                });
                setAuthors(Array.from(temp, ([nationality, value]) => ({'Nationality': nationality, 'Authors': value})));
                setIsLoading(false);
            });
        }
        getData();
    }, []);

    return (
        <div>
            <h3 className="borderedHeader">
                Authors by Nationality
            </h3>
            <br/>
            {isLoading ? (
                <div class="d-flex justify-content-center">
                    <Spinner animation="border" role="status">
                        <span className="visually-hidden">Loading...</span>
                    </Spinner>
                </div>
            ) : (
                <div className="d-flex justify-content-center">
                    <BarChart
                        width={1200}
                        height={250}
                        data={authorsByNationality}
                        margin={{
                            top: 5,
                            right: 30,
                            left: 20,
                            bottom: 5,
                        }}
                        // layout='vertical'
                    >
                        <CartesianGrid strokeDasharray="3 3"/>
                        <XAxis dataKey="Nationality"/>
                        <YAxis/>
                        <Tooltip/>
                        <Legend/>
                        <Bar type="monotone" dataKey="Authors" fill="#0F9D58"/>
                    </BarChart>
                </div>)}
        </div>
    );
}

export default AuthorVisualization;