import React, { useEffect, useState } from 'react';
import {PieChart,Cell, Pie,BarChart,CartesianGrid,XAxis,YAxis,Tooltip,Legend,Bar, ScatterChart,ZAxis,Scatter,RadarChart,PolarGrid,PolarAngleAxis,PolarRadiusAxis,Radar} from 'recharts';
import axios from "axios";
import { Spinner } from 'react-bootstrap';
function CityVisualization(){
  const [sorted,setSorted] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  useEffect(() => {
      const getData = async () => {
          console.log("changing");
          const response1 = await axios.get("https://api.atraveltx.me/api/city?s=populationwithasc");
          setSorted(response1.data.result);
          setIsLoading(false);
      }
      getData();
  }, []);
  return(
    <div>
    <h3 class="borderedHeader">
        Population by City
    </h3>
    {isLoading ? (
            <div class="d-flex justify-content-center">
                <Spinner animation="border" role="status">
                    <span className="visually-hidden">Loading...</span>
                </Spinner>
            </div>
        ) : (
          <div class="d-flex justify-content-center">
            <ScatterChart width={1200} height={250}
              margin={{ top: 5, right: 30, bottom: 20, left: 5 }}>
              <CartesianGrid strokeDasharray="3 3" />
              <XAxis dataKey="population" name="population"/>
              <YAxis dataKey="size" name="size" />
              <ZAxis dataKey="city_name"  name="city_name"/>
              <Tooltip cursor={{ strokeDasharray: '3 3' }} />
              <Legend />
              <Scatter name="city" data={sorted} fill="#8884d8" />
            </ScatterChart>
          </ div>
      )}
    </ div>
  )
}


export default CityVisualization;
