import React, {useEffect, useState} from "react";
import axios from "axios";
import {BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer} from 'recharts';
import {Card, Row, Spinner} from "react-bootstrap";


function CovidVisualization() {
    const [covidByCounty, setCovidData] = useState([]);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        const getData = async () => {
            axios.get('https://api.atraveltx.me/api/covid').then((response) => {
                const temp = [];
                response.data.result.forEach(county => {
                    temp.push({
                        'County': county.county_name,
                        'Cases (%)': Math.round(county.cases / county.population * 1000) / 10
                    });
                });
                setCovidData(temp);
                setIsLoading(false);

            });
        }
        getData();
    }, []);

    return (
        <div>
            <h3 class="borderedHeader">
                Cases over population, by County
            </h3>
            <br/>
            {isLoading ? (
                <div class="d-flex justify-content-center">
                    <Spinner animation="border" role="status">
                        <span className="visually-hidden">Loading...</span>
                    </Spinner>
                </div>
            ) : (
                <div class="d-flex justify-content-center">
                    <BarChart
                        width={1200}
                        height={250}
                        data={covidByCounty}
                        margin={{
                            top: 5,
                            right: 30,
                            left: 20,
                            bottom: 5,
                        }}
                    >
                        <CartesianGrid strokeDasharray="3 3"/>
                        <XAxis dataKey="County"/>
                        <YAxis domain={[0, 100]}/>
                        <Tooltip/>
                        <Legend/>
                        <Bar type="monotone" dataKey="Cases (%)" fill="#4285F4"/>
                    </BarChart>
                </div>
            )}
        </div>
    );
}

export default CovidVisualization;