import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { Spinner } from 'react-bootstrap';
import BubbleChart from '@weknow/react-bubble-chart-d3';

function FlightVisualization () {
    const [isLoading, setIsLoading] = useState(true);
    const [displayedData, setDisplayedData] = useState([]);
    
    useEffect(() => {
        const getData = async () => {
            const response = await axios.get("https://api.atraveltx.me/api/flight");

            // Create mapping for counting number of airlines
            const mapping = new Map();
            response.data.result.forEach((flight) => {
                if (mapping.get(flight.airline_name)) {
                    mapping.set(flight.airline_name, mapping.get(flight.airline_name) + 1);
                } else {
                    mapping.set(flight.airline_name, 1);
                }
            });

            // Convert to format [{label:, value:}]
            let temp = Array.from(mapping, ([label, value]) => ({ label, value }));
            setDisplayedData(temp);
            setIsLoading(false);
        }
        getData();
    },[]);

    return (
        <div>
            <h3 class="borderedHeader">Flights by Airline</h3>
            <br/>
            {isLoading ? (
                    <div class="d-flex justify-content-center">
                        <Spinner animation="border" role="status">
                            <span className="visually-hidden">Loading...</span>
                        </Spinner>
                    </div>
                ) : (
                    <div class="d-flex justify-content-center">
                    <BubbleChart
                        graph={{
                            zoom: 0.75,
                            offsetX: 0.0,
                            offsetY: 0.0,
                        }}
                        legendPercentage={20}
                        width={1100}
                        height={700}
                        valueFont={{
                            family: "Arial",
                            size: 12,
                            color: "#fff",
                            weight: "bold",
                        }}
                        labelFont={{
                            family: "Arial",
                            size: 16,
                            color: "#fff",
                            weight: "bold",
                        }}
                        data={displayedData}
                    />
                    </div>
                )}
        </div>
    );
}

export default FlightVisualization;