import { Container } from "react-bootstrap";
import FlightVisualization from "./FlightVisualization";
import CovidVisualization from "./CovidVisualization";
import CityVisualization from "./CityVisualization";

function Visualization () {
    return (
        <div>
            <Container>
                <h1>Visualizations</h1>
                <br/>
                <FlightVisualization/>
                <br/>
                <CovidVisualization/>
                <br />
                <CityVisualization/>
            </Container>
        </div>
    );
}

export default Visualization;
