import os
from sys import platform

if __name__ == "__main__":
    # Use chromedriver based on OS
    if platform == "win32":
        PATH = "./selenium/chromedriver.exe"
    elif platform == "linux":
        PATH = "./selenium/chromedriver_linux"
    else:
        print("Unsupported OS")
        exit(-1)

    # Run all of the gui tests
    os.system("python3 ./selenium/test.py " + PATH)
