import unittest
import time
import sys
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.chrome.options import Options
import sys

PATH = "./selenium/chromedriver.exe"

class SeleniumTests(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        chrome_options = Options()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-dev-shm-usage')
        cls.driver = webdriver.Chrome(PATH, options=chrome_options)

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

    def test1(self):
        self.driver.get('https://www.atraveltx.me/')
        result = self.driver.find_element_by_css_selector('#root')
        self.assertNotEqual(result, None)

    def test2(self):
        self.driver.get('https://www.atraveltx.me/')
        result = self.driver.find_element_by_class_name('positionedLogo')
        self.assertNotEqual(result, None)

    def test3(self):
        self.driver.get('https://www.atraveltx.me/')
        WebDriverWait(self.driver, 4).until(ec.presence_of_all_elements_located((By.CLASS_NAME, 'card')))
        result = self.driver.find_elements_by_class_name('card')
        self.assertEqual(len(result), 3)

    def test4(self):
        self.driver.get('https://www.atraveltx.me/')
        WebDriverWait(self.driver, 4).until(ec.presence_of_all_elements_located((By.XPATH, '//a[@href="'+'/flight'+'"]')))
        result = self.driver.find_elements_by_xpath('//a[@href="'+'/flight'+'"]')
        self.assertEqual(len(result), 2)
        self.driver.find_elements_by_class_name('card')[0].click()
        self.assertEqual(self.driver.current_url, 'https://www.atraveltx.me/flight/')

    def test5(self):
        self.driver.get('https://www.atraveltx.me/')
        WebDriverWait(self.driver, 4).until(ec.presence_of_all_elements_located((By.XPATH, '//a[@href="'+'/cityModule'+'"]')))
        result = self.driver.find_elements_by_xpath('//a[@href="'+'/cityModule'+'"]')
        self.assertEqual(len(result), 2)
        self.driver.find_elements_by_class_name('card')[1].click()
        self.assertEqual(self.driver.current_url, 'https://www.atraveltx.me/cityModule/')

    def test6(self):
        self.driver.get('https://www.atraveltx.me/')
        WebDriverWait(self.driver, 4).until(ec.presence_of_all_elements_located((By.XPATH, '//a[@href="'+'/covid'+'"]')))
        result = self.driver.find_elements_by_xpath('//a[@href="'+'/covid'+'"]')
        self.assertEqual(len(result), 2)
        self.driver.find_elements_by_class_name('card')[2].click()
        self.assertEqual(self.driver.current_url, 'https://www.atraveltx.me/covid/')

    def test7(self):
        self.driver.get('https://www.atraveltx.me/')
        result = self.driver.find_element_by_xpath('//a[@href="'+'/about'+'"]')
        self.assertNotEqual(result, None)
        
    def test8(self):
        self.driver.get('https://www.atraveltx.me/flight')
        result = self.driver.find_element_by_tag_name('h1').get_attribute('innerHTML')
        self.assertEqual(result, 'Flights to Texas')

    def test9(self):
        self.driver.get('https://www.atraveltx.me/cityModule')
        result = self.driver.find_element_by_tag_name('h1').get_attribute('innerHTML')
        self.assertEqual(result, 'City')

    def test10(self):
        self.driver.get('https://www.atraveltx.me/covid')
        result = self.driver.find_element_by_class_name('card-title').get_attribute('innerHTML')
        self.assertEqual(result, 'Covid Infection Rates by County')

    def test11(self):
        self.driver.get('https://www.atraveltx.me/search')
        result = self.driver.find_element_by_tag_name('h1').get_attribute('innerHTML')
        self.assertEqual(result, 'Search')
        result = self.driver.find_elements_by_tag_name('h3')
        self.assertEqual(len(result), 3)

    def test12(self):
        self.driver.get('https://www.atraveltx.me/flight/?page=1&pagesize=20&s=departure_icaowithasc')
        time.sleep(2)
        result = self.driver.find_elements_by_class_name('card')
        self.assertEqual(len(result), 20)

    def test13(self):
        self.driver.get('https://www.atraveltx.me/flight/?page=1&pagesize=20&s=departure_icaowithasc')
        time.sleep(2)
        result = self.driver.find_element_by_class_name('form-control')
        result.send_keys('shouldhavenoresults')
        result.send_keys(Keys.ENTER)
        time.sleep(2)
        result = self.driver.find_elements_by_class_name('card')
        self.assertEqual(len(result), 0)

    def test14(self):
        self.driver.get('https://www.atraveltx.me/flight/?page=1&pagesize=20&s=departure_icaowithasc')
        time.sleep(2)
        result = self.driver.find_element_by_class_name('form-control')
        result.send_keys('hel')
        result.send_keys(Keys.ENTER)
        time.sleep(2)
        result = self.driver.find_elements_by_class_name('card')
        self.assertEqual(len(result), 2)
        result = self.driver.find_elements_by_class_name('page-link')
        self.assertEqual(len(result), 3)

    def test15(self):
        self.driver.get('https://www.atraveltx.me/search')
        time.sleep(2)
        result = self.driver.find_elements_by_class_name('card')
        self.assertEqual(len(result), 33)

    def test16(self):
        self.driver.get('https://www.atraveltx.me/search')
        time.sleep(2)
        result = self.driver.find_element_by_class_name('form-control')
        result.send_keys('shouldhavenoresults')
        result.send_keys(Keys.ENTER)
        time.sleep(2)
        result = self.driver.find_elements_by_class_name('card')
        self.assertEqual(len(result), 0)

    def test17(self):
        self.driver.get('https://www.atraveltx.me/covid/?page=1&pagesize=20')
        time.sleep(2)
        result = self.driver.find_elements_by_tag_name('tr')
        self.assertEqual(len(result), 21)

    def test18(self):
        self.driver.get('https://www.atraveltx.me/covid/?page=1&pagesize=20')
        time.sleep(2)
        result = self.driver.find_element_by_class_name('form-control')
        result.send_keys('shouldhavenoresults')
        result.send_keys(Keys.ENTER)
        time.sleep(2)
        result = self.driver.find_elements_by_tag_name('tr')
        self.assertEqual(len(result), 1)

    def test19(self):
        self.driver.get('https://www.atraveltx.me/covid/?page=1&pagesize=20')
        time.sleep(2)
        result = self.driver.find_element_by_class_name('form-control')
        result.send_keys('col')
        result.send_keys(Keys.ENTER)
        time.sleep(2)
        result = self.driver.find_elements_by_tag_name('tr')
        self.assertEqual(len(result), 5)

    def test20(self):
        self.driver.get('https://www.atraveltx.me/cityModule')
        time.sleep(2)
        result = self.driver.find_elements_by_tag_name('tr')
        self.assertEqual(len(result), 11)

    def test21(self):
        self.driver.get('https://www.atraveltx.me/citySearch')
        time.sleep(2)
        result = self.driver.find_element_by_tag_name('input')
        result.send_keys('shouldhavenoresults')
        self.driver.find_element_by_xpath('//button[contains(text(), "Search City")]').click()
        time.sleep(2)
        result = self.driver.find_elements_by_tag_name('card')
        self.assertEqual(len(result), 0)

    def test22(self):
        self.driver.get('https://www.atraveltx.me/flight/?page=1&pagesize=20&di=KABE')
        time.sleep(2)
        self.driver.find_element_by_class_name('card').click()
        time.sleep(2)
        result = self.driver.find_elements_by_xpath('//td[contains(text(), "KABE")]')
        self.assertNotEqual(result, None)

    def test23(self):
        self.driver.get('https://www.atraveltx.me/covid/?page=1&pagesize=20&s=county_namewithasc&tl=l')
        time.sleep(2)
        result = self.driver.find_elements_by_tag_name('tr')
        self.assertEqual(len(result), 6)

    def test24(self):
        self.driver.get('https://www.atraveltx.me/cityModule')
        time.sleep(2)
        self.driver.find_elements_by_id('dropdown-basic-button')[0].click()
        self.driver.find_element_by_xpath('//a[contains(text(), "6%-6.5%")]').click()
        time.sleep(2)
        result = self.driver.find_element_by_xpath('//td[contains(text(), "Aldine")]')
        self.assertNotEqual(result, None)

if __name__ == '__main__':
    PATH = sys.argv[1]
    unittest.main(argv=['first-arg-is-ignored'])
